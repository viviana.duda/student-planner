<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ExamsController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\ScheduleController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\NotificationsController;
use App\Http\Middleware\CacheInvalidator;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('guest')->group(function() {
    Route::get('/register', [UserController::class, 'register'])->name('register.show');
    Route::post('/register', [UserController::class, 'store'])->name('register.store');
    Route::get('/login', [UserController::class, 'login'])->name('login.show');
    Route::post('/login', [UserController::class, 'authenticate'])->name('authenticate');
});

Route::middleware('auth')->group(function(){
    
    Route::get('/exams/{filter?}', [ExamsController::class, 'index'])->name('exam.show');
    Route::prefix('exam')->group(function() {
        Route::get('create', [ExamsController::class, 'create'])->name('exam.create');
        Route::get('/{examId}/remove', [ExamsController::class, 'remove'])->name('exam.remove');
        Route::get('/{examId}/status', [ExamsController::class, 'updateStatus'])->name('exam.updateStatus');
        Route::post('/store', [ExamsController::class, 'store'])->name('exam.store');
        Route::get('/{examId}/edit', [ExamsController::class, 'edit'])->name('exam.edit');
        Route::put('/{examId}', [ExamsController::class, 'update'])->name('exam.update');
    });

    Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index');
    Route::prefix('task')->group(function() {
        Route::get('/create', [TaskController::class, 'create'])->name('task.create');
        Route::get('/{taskId}/overview', [TaskController::class, 'showOverview'])->name('task.overview');
        Route::post('/store', [TaskController::class, 'store'])->name('task.store');
        Route::get('/{taskId}/edit', [TaskController::class, 'edit'])->name('task.edit');
        Route::put('/{taskId}', [TaskController::class, 'update'])->name('task.update');
        Route::delete('/{taskId}/removeFile/{fileId}', [TaskController::class, 'removeFile'])->name('task.removeFile');
        Route::get('/{taskId}/downloadFile/{fileId}', [TaskController::class, 'downloadFile'])->name('task.downloadFile');
        Route::get('/{taskId}/remove', [TaskController::class, 'remove'])->name('task.remove');
        Route::put('/{taskId}/progress', [TaskController::class, 'updateProgress'])->name('task.updateProgress');
        Route::get('/{taskId}/files', [TaskController::class, 'getFiles'])->name('tasks.files.index');
    });

    Route::prefix('schedule')->group(function() {
        Route::get('', [ScheduleController::class, 'show'])->name('schedule.show');
        Route::put('', [ScheduleController::class, 'update'])->name('schedule.update');
        Route::get('/items', [ScheduleController::class, 'latestScheduleItems'])->name('schedule.items');
    });

    Route::get('/profile', [UserController::class, 'showProfile'])->name('user.showProfile')->middleware(CacheInvalidator::class);
    Route::prefix('user')->group(function() {
        Route::post('', [UserController::class, 'update'])->name('user.update');
        Route::post('/getEvents', [UserController::class, 'getEvents'])->name('user.getEvents');
    });

    Route::prefix('notifications')->group(function() {
        Route::get('/', [NotificationsController::class, 'getNotifications'])->name('getNotifications');
        Route::post('/acknowledge', [NotificationsController::class, 'acknowledge'])->name('acknowledge');
    });

    Route::get('/logout', [UserController::class, 'logout'])->name('logout');
    Route::get('/dashboard',  [DashboardController::class, 'show'])->name('dashboard.show');
    Route::get('/calendar',[CalendarController::class, 'show'])->name('calendar.show');
    Route::get('/test',function() {
        return view('test');
    })->name('calendar.show');
    Route::get('/', function(){
        return view('home', [
            "calendarData" => User::find(auth()->id())->calendarData()
        ]);
    })->middleware(CacheInvalidator::class);
});