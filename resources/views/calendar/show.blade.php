  @push('scripts')
      <link rel="stylesheet" href="{{asset('css/fullcalendar.min.css')}}">
      <link rel="stylesheet" href="{{asset('css/semantic.min.css')}}">
      <link rel="stylesheet" href="{{asset('css/fullcalendar.min.css')}}">
      <link rel="stylesheet" href="{{asset('css/calendar.css')}}">

      <script src="{{asset('js/semantic.min.js')}}"></script>
      <script src="{{asset('js/jquery.min.js')}}"></script>
      <script src="{{asset('js/moment.min.js')}}"></script>
      <script src="{{asset('js/fullcalendar.min.js')}}"></script>
  @endpush

<x-layout>
  <script>
      let currentDate = dayjs();
      $(document).ready(function() {
      $('#calendar').fullCalendar({
          header: {
              center: 'prev,title,next',
              left: '',
              right: '  '
          },
          defaultDate: currentDate,
          defaultView: 'basicWeek',
          eventLimit: true,
          events: @json($calendarData),
          timeFormat: 'h:mm a'
      });
});

  </script>
    <div style="max-height: 80%;" class="mt-5">
      <div class="ui container">
        <div class="ui grid">
          <div class="ui sixteen column">
              <div id="calendar">
              </div>
          </div>
        </div>
      </div>
    </div>
</x-layout>





   
