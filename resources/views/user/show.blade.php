@push('scripts')
    <link href="{{asset('css/user/profile.css')}}" rel="stylesheet">
@endpush

@php 
    $user = auth()->user();
    $userProfileImagePath = $layoutData['userProfileImagePath'];
@endphp

<x-layout>
    <div id="user-profile-main" class="container-fluid mt-5 ms-4 w-100"> <!-- User profile main -->
        <div class="row">
            <div class="col-7 col-sm-6 col-md-6 col-lg-5 col-xl-4">
                <form class="d-flex flex-column" action="/user" method="POST" enctype="multipart/form-data"> <!-- Profile form -->
                    @csrf
                    @method('POST')
        
                    <div class="container d-flex flex-column align-items-center">
                        @if(isset($userProfileImagePath))
                            <img id="profile-image" src="{{ Storage::url($userProfileImagePath) }}?timestamp={{ time() }}">    
                        @else 
                            <img id="profile-image" src="{{asset('images/default-avatar.png')}}?timestamp={{ time() }}">    
                        @endif

                        <label class="overlay" for="file-upload">
                            <img class="overlay-image" src="images/edit-profile-img.svg">
                        </label>

                        <input id="file-upload" class="hidden" type="file" accept="image/*" name="profile_image">
                    </div>
        
                    <div class="form-group">
                        <label>Nume</label>
                        <input type="text" class="form-control form-field" name="name" placeholder="John Doe" value="{{$user->name}}">
                        @error('name')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <label>Facultate</label>
                        <select class="form-control form-field" name="faculty_id" value="4">
                            @foreach ($faculties as $faculty)
                                @if($faculty->id == $user->faculty_id)
                                    <option value="{{$faculty->id}}" selected>{{$faculty->name}}</option>    
                                @else
                                    <option value="{{$faculty->id}}">{{$faculty->name}}</option>    
                                @endif
                            @endforeach                                
                        </select>
                        @error('faculty_id')
                        <div class="alert alert-danger" role="alert">
                            {{$message}}
                        </div>     
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <label>Parola</label>
                        <input type="password" class="form-control form-field" name="password" placeholder="********">
                        @error('password')
                            <div class="alert alert-danger" role="alert">
                                {{$message}}
                            </div>     
                        @enderror
                    </div>
            
                    <div class="d-flex justify-content-center">
                        <input id="update-profile-button" type="submit" class="btn btn-primary btn-lg mt-3" value="Modifica">
                    </div>
                </form> <!-- Profile form -->
            </div>
        </div>
    </div> <!-- User profile main -->
</x-layout> 