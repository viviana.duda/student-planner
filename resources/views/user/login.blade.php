<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> Login | My student planner</title>

        {{-- Bootstrap --}}
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

        {{-- Custom  --}}
        <link rel="stylesheet" href="{{asset('css/login.css')}}">
        <script src="{{asset('js/user/clientInformationsLogin.js')}}"></script>

        <!-- Dayjs -->
        <script src="{{asset('js/dayjs.min.js')}}"></script>
        <script src="{{asset('js/timezone.min.js')}}"></script>

         <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
    </head>

    <script>
        dayjs.extend(window.dayjs_plugin_timezone);
    </script>

    <body>
        <div id="login-page-container" class="d-flex"> <!-- Login Page Container -->
            <div id="login-form-container" class="d-flex flex-column justify-content-center align-items-center w-100"> <!-- Login Form -->
                    
                <object class ="small-circle-svg" type="image/svg+xml" data="{{asset('images/small-circle.svg')}}">
                    Your browser does not support SVG
                </object>

                <h1 id="title">Student Planner</h1>

                <h1 id="sub-title">Autentificare in cont</h1>

                <form id='login-form' action="/login" method="POST" onsubmit='clientInformationsLogin(event)'>
                    @csrf

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" placeholder="user@email.com" value="{{old('email')}}">
                        @error('email')
                            <div class="alert alert-danger" role="alert">
                                {{$message}}
                            </div>               
                        @enderror
                    </div>

                    <div class="form-group">
                        <label>Parola</label>
                        <input type="password" class="form-control" name="password" placeholder="********" value="{{old('password')}}">
                        @error('password')
                            <div class="alert alert-danger" role="alert">
                                {{$message}}
                            </div>     
                        @enderror
                    </div>

                    <input id="login-button" type="submit" class="btn btn-primary btn-lg mt-3 ms-5" value="CONECTARE">
                </form>

                <div id="form-footer" class="mt-4 ms-5 ps-3 me-2">
                    <h6 class="d-inline">Nu ai un cont?</h6>
                    <a id="register-button" class="btn btn-primary d-inline ms-2" href="/register">Inregistrati-va</a>
                </div>
                
            </div> <!-- Login Form -->

            <div id="right-panel"> <!-- Right Panel -->
                <div id="right-panel-content">
                    <object class ="blue-square-svg" type="image/svg+xml" data="{{asset('images/blue-square.svg')}}">
                        Your browser does not support SVG
                    </object>

                    <object class ="transparent-circle-svg" type="image/svg+xml" data="{{asset('images/transparent-circle.svg')}}">
                        Your browser does not support SVG
                    </object>

                    <object class ="blue-circle-svg" type="image/svg+xml" data="{{asset('images/blue-circle.svg')}}">
                        Your browser does not support SVG
                    </object>

                    <object class ="bordered-circle-svg" type="image/svg+xml" data="{{asset('images/bordered-circle.svg')}}">
                        Your browser does not support SVG
                    </object>

                    <object class ="suit-case-svg"type="image/svg+xml" data="{{asset('images/suit-case.svg')}}">
                        Your browser does not support SVG
                    </object>
                </div>
            </div> <!-- Right Panel -->
        </div> <!-- Login Page Container -->
    </body> 
</html>