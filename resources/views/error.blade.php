<!DOCTYPE html>
<html>
<head>
    <title>500 - Internal Server Error</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #3498db ;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 90vh;
        }

        .container {
            max-width: 400px;
            background-color: #FF8C00;
            color: #fff;
            padding: 30px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            text-align: center;
            animation: fadeIn 0.5s ease-in-out;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 20px;
        }

        p {
            margin-bottom: 20px;
        }

        a {
            color: #fff;
            text-decoration: none;
            font-weight: bold;
            display: inline-block;
            animation: bounce 0.8s infinite;
        }

        @keyframes fadeIn {
            0% {
                opacity: 0;
            }
            100% {
                opacity: 1;
            }
        }

        @keyframes bounce {
            0%, 20%, 50%, 80%, 100% {
                transform: translateY(0);
            }
            40% {
                transform: translateY(-10px);
            }
            60% {
                transform: translateY(-5px);
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Oops!</h1>
        <p>Ceva nu a functionat bine pe partea noastra.</p>
        <p>Te rog sa reincerci din nou sau sa contactezi suportul.</p>
        <a href="/">Spre pagina principala</a>
    </div>
</body>
</html>
