@push('scripts')
    <link href="{{ asset('css/exams/index.css') }}" rel="stylesheet">
    <script src="{{ asset('js/exams/displayCardMenu.js')}}"></script>
    <script src="{{ asset('js/exams/filterExams.js')}}"></script>
    <script src="{{ asset('js/exams/ackDeleteExam.js')}}"></script>
    <script src="{{ asset('js/exams/ackExamPassed.js')}}"></script>
    <script src="{{ asset('js/exams/markExamPassed.js')}}"></script>
    <script src="{{ asset('js/exams/removeExam.js')}}"></script>
@endpush

<x-layout>

    <div class="modal fade exam-passed-modal" id="examPassedModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">  <!-- Check Exam as Passed modal -->
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content" style="max-width: 400px;">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Ai promovat examenul?</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
               
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Nu</button>
                    <button id="exam-passed-modal-button" type="button" class="btn btn-primary" data-bs-dismiss="modal">Da</button>
                </div>
            </div>
        </div>
    </div> <!-- Check Exam as Passed modal -->

    <div class="modal fade remove-exam-modal" id="removeExamModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"> <!-- Remove exam modal -->
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content" style="max-width: 400px;">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Esti sigur?</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
               
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Inchide</button>
                    <button id="remove-exam-modal-button" type="button" class="btn btn-primary" data-bs-dismiss="modal">Sterge</button>
                </div>
            </div>
        </div>
    </div> <!-- Remove exam modal -->

    <div id="exams-main" class="d-flex flex-column w-100 h-100"> <!-- Exams main -->
        <div id="exams-main-header" class="d-flex justify-content-start align-items-center ms-5 ps-lg-5 mt-1 mt-xl-5"> <!-- Exams header -->
            <h2 id="header-title" class="fw-bold">Examene</h2>
            <a class="btn btn-link mb-2 ps-1 ps-xl-3" href="/exam/create"><img id="create-exam-icon" src="{{asset('images/plus-sign.svg')}}"></a>
            <div id="filter-exams-container" class="d-flex flex-column justify-content-center align-items-center ms-auto me-3 me-xl-4 pb-2">
                <button id="filter-button" class="border-0 bg-transparent" onclick="filterExams()">
                    <img id='filter-button-image' src="{{asset('images/filter-icon.svg')}}">
                </button>
            
                <div id="filter-buttons-container" class="d-flex flex-column justify-content-center align-items-center me-1">
                </div>
            </div>
        </div> <!-- Exams header -->

        <div id="exams-container" class="container-fluid ms-3 ps-1 mt-3 ms-lg-5 ps-md-5"> <!-- Exams container -->
            @foreach($exams as $exam)
                <div class="row mt-1 mb-4">
                    <div class="col-9 col-sm-8 col-md-7 col-lg-6 col-xl-6 d-inline-block">
                        <div id="card{{$exam['id']}}" class="exam-card card">
                            <div class="card-body d-flex flex-column ms-3">
                                <div class="d-flex justify-content-between">
                                    <h5 class="card-title mt-2">{{$exam['course_name']}}</h5>
                                    <div onclick="displayCardMenu('{{$exam['id']}}')">
                                        <img src="{{asset('images/three-dots-icon.svg')}}"  class="ms-3 options-box-icon">
                                    </div>     
                                </div>
    
                                <div class="d-flex mt-3 mt-xxl-3 pt-xxl-2 justify-content-between">
                                    <small id="exam-date" class="text-nowrap">Data sustinerii {{$exam['date']}}</small>
                                    <small id="exam-duration"> {{$exam['duration']}} minute</small>
                                </div>
                              
                                <small id="exam-start-hour" >{{$exam['start_hour']}}</small> 
                            </div>
                        </div>
                    </div>
                    <div class="col-1 flex-column justify-content-center" id="card{{$exam['id']}}-options-box" style="display: none; width: 100px;">
                        {{-- Options box container --}}
                    </div>
                </div>
            @endforeach
        </div> <!-- Exams container -->
    
        <nav id="navigation-container" class="mt-auto align-self-center"> <!-- Exams navigation footer -->
            <ul class="pagination">
                @if($exams->onFirstPage())
                    <li class="page-item disabled">
                    <a id="previous-page-button-disabled" class="page-link" href="#" tabindex="-1">Inapoi</a>
                @else
                    <li class="page-item active">
                    <a id="previous-page-button" class="page-link" href="{{$exams->previousPageUrl()}}" tabindex="-1">Inapoi</a>
                @endif
            </li>

            <li class="page-item">
                @if($exams->nextPageUrl())
                    <a id="next-page-button" class="page-link" href="{{$exams->nextPageUrl()}}">Urmatoarea</a>
                @else
                    <a id="next-page-button-disabled" class="page-link disabled" href="{{$exams->nextPageUrl()}}">Urmatoarea</a>
                @endif
            </li>

            </ul>
        </nav> <!-- Exams navigation footer --> 
    </div> <!-- Exams main -->
</x-layout>