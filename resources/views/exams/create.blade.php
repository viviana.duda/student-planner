@push('scripts')
    <link href="{{asset('css/exams/create.css')}}" rel="stylesheet">
@endpush

<x-layout>
    <div id="exams-create-main" class="d-flex flex-column justify-content-center align-items-center h-100 w-100">  <!-- Exams create main -->
        <form id="exams-create-form" class="d-flex flex-column" action="/exam/store" method="POST"> <!-- Exams create form -->
            @csrf
            @method('POST')

            <div class="container-fluid">
                <div class="row m-auto ms-5 me-5 mt-5">
                    <div id="left-part" class="col-6">
                        <div class="d-flex flex-column">
                            <div class="mb-4 w-100">
                                @if($errors->has('course_name'))
                                    <div id="course-name-input-container" class="form-group" style="border-color: red;">
                                    <input type="text" class="form-control bg-transparent border-0" name="course_name" placeholder="{{$errors->first('course_name')}}">
                                @else
                                    <div id="course-name-input-container" class="form-group">
                                    <input type="text" class="form-control bg-transparent border-0"  value="{{old('course_name')}}" name="course_name" placeholder="Materia">
                                @endif
                                    </div>
                            </div>

                            <div class="mb-4">
                                @if($errors->has('date'))
                                    <div id="date-input-container" class="form-group" style="border-color: red;">
                                    <input id="examCreateDateTimePicker" type="text" class="form-control bg-transparent border-0" name="date" placeholder="{{$errors->first('date')}}">
                                @else
                                    <div id="date-input-container" class="form-group" >
                                    <input id="examCreateDateTimePicker" type="text" class="form-control bg-transparent border-0" value="{{old('date')}}" name="date" placeholder="Data">
                                @endif
                                    </div>
                            </div>

                            <div class="mb-4">
                                @if($errors->has('class_name'))
                                    <div id="class_name-input-container" class="form-group" style="border-color: red;">
                                    <input type="text" class="form-control bg-transparent border-0" name="class_name" placeholder="{{$errors->first('class_name')}}">
                                @else
                                    <div id="class_name-input-container" class="form-group">
                                    <input type="text" class="form-control bg-transparent border-0" value="{{old('class_name')}}" name="class_name" placeholder="Sala">
                                @endif
                                    </div>     
                            </div>
                        </div>
                    </div>

                    <div id="right-part" class="col-6">
                        <div class="d-flex flex-column">
                            <div class="mb-4">
                                @if($errors->has('duration'))
                                    <div id="duration-input-container" class="form-group" style="border-color: red;">
                                    <input type="number" class="form-control bg-transparent border-0" min="0" name="duration" placeholder="{{$errors->first('duration')}}">
                                @else
                                    <div id="duration-input-container" class="form-group">
                                    <input type="number" class="form-control bg-transparent border-0" min="0" value="{{old('duration')}}" name="duration" placeholder="Durata (minute)">
                                @endif
                                    </div>    
                            </div>
                                
                            <div class="mb-4">
                                @if($errors->has('teacher_name'))
                                    <div id="teacher_name-input-container" class="form-group" style="border-color: red;">
                                    <input type="text" class="form-control bg-transparent border-0"  name="teacher_name" placeholder="{{$errors->first('teacher_name')}}">
                                @else
                                    <div id="teacher_name-input-container" class="form-group">
                                    <input type="text" class="form-control bg-transparent border-0" value="{{old('teacher_name')}}" name="teacher_name" placeholder="Profesor">
                                @endif
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="color-picker" class="align-self-center mt-auto">
                <img src="{{asset('images/color-picker.svg')}}" id="color-picker-icon">
                <input id="calendar-color-input" type="color" name="calendar_color">
            </div>
         
            <div class="form-buttons ms-auto me-3 me-lg-5 mt-auto mb-4">
                <a id="cancel-button" class="btn btn-primary btn-lg" href="/exams">Cancel</a>
                <input id="save-exam-button" class="btn btn-primary btn-lg ms-1 ms-md-2" type="submit" value="Salveaza">
            </div>
        </form> <!-- Exams create form -->
    </div> <!-- Exams create main -->
    
    <script>
        flatpickr("#examCreateDateTimePicker", {
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            time_24hr: true,
        });
    </script>
</x-layout>