@push('scripts')
    <link href="{{asset('css/tasks/edit.css')}}" rel="stylesheet">
@endpush

<x-layout>
    <div id="tasks-edit-main" class="d-flex flex-column justify-content-center align-items-center h-100 w-100">  <!-- Tasks edit main -->
        <form id="tasks-edit-form" class="d-flex flex-column" action="/task/{{$task['id']}}" method="POST" enctype="multipart/form-data"> <!-- Tasks edit form -->
            @csrf
            @method('PUT')

            <div class="container-fluid">
                <div class="row m-auto ms-5 me-5 mt-5">
                    <div id="left-part" class="col-6">
                        <div class="d-flex flex-column">
                            <div class="mb-4 w-100">
                                @if($errors->has('title'))
                                    <div id="title-input-container" class="form-group" style="border-color: red;">
                                    <input type="text" class="form-control bg-transparent border-0" name="title" placeholder="{{$errors->first('title')}}">
                                @else
                                    <div id="title-input-container" class="form-group">
                                    <input type="text" class="form-control bg-transparent border-0"  value="{{$task['title']}}" name="title" placeholder="Titlu">
                                @endif
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div id="right-part" class="col-6">
                        <div class="d-flex flex-column">
                            <div class="mb-4 w-100">
                                @if($errors->has('course'))
                                    <div id="course-input-container" class="form-group" style="border-color: red;">
                                    <input type="text" class="form-control bg-transparent border-0" name="course" placeholder="{{$errors->first('course')}}">
                                @else
                                    <div id="course-input-container" class="form-group">
                                    <input type="text" class="form-control bg-transparent border-0" value="{{$task['course']}}" name="course" placeholder="Materia">
                                @endif
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div id="center-part" class="col-12 d-flex justify-content-center flex-wrap">
                        <div class="d-flex flex-column w-50 d-block mb-4">
                            @if($errors->has('deadline'))
                                <div id="deadline-input-container" class="form-group" style="border-color: red;">
                                    <input id="taskEditTimePicker" type="text" class="form-control bg-transparent border-0" name="deadline" placeholder="{{$errors->first('deadline')}}">
                            @else
                                <div id="deadline-input-container" class="form-group">
                                    <input id="taskEditTimePicker" type="text" class="form-control bg-transparent border-0" value="{{$task['deadline']}}" name="deadline" placeholder="Deadline">
                            @endif
                                </div>
                        </div>
                    
                        <div class="d-flex flex-column w-100">
                            @if($errors->has('description'))
                                <div id="description-input-container" class="form-group" style="border-color: red;">
                                <textarea type="text" class="form-control bg-transparent border-0" name="description" placeholder="{{$errors->first('description')}}"></textarea>
                            @else
                                <div id="description-input-container" class="form-group" style="">
                                <textarea type="text" class="form-control bg-transparent border-0" name="description" placeholder="Ce am de facut?">{{$task['description']}}</textarea>
                            @endif
                                </div>
                        </div>

                        <div class="d-flex flex-column w-100 mt-3 mt-xl-5">
                            <div class="form-group text-center" style="display: flex; flex-direction: column; align-items: center;">
                                <label for="file-input" class="custom-file-input-label" id="file-label">Nici-un fisier selectat &nbsp&nbsp</label>
                                <input id="file-input" type="file" class="d-none form-control bg-transparent border-0" name="files[]" multiple>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         
            <div class="form-buttons ms-auto me-3 me-lg-5 mt-auto mb-4">
                <a id="cancel-button" class="btn btn-primary btn-lg" href="/tasks">Cancel</a>
                <input id="save-task-button" class="btn btn-primary btn-lg ms-1 ms-md-2" type="submit" value="Salveaza">
            </div>
        </form> <!-- Exams edit form -->
    </div> <!-- Tasks edit main -->
    
    <script>
        const fileInput = document.getElementById('file-input');
        const fileLabel = document.getElementById('file-label');

        fileInput.addEventListener('change', function() {
            if (fileInput.files.length > 0) {
                fileLabel.textContent = fileInput.files[0].name;
            } else {
                fileLabel.textContent = 'Nici-un fisier selectat';
            }
        });

        flatpickr("#taskEditTimePicker", {
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            time_24hr: true,
        });
    </script>
</x-layout>