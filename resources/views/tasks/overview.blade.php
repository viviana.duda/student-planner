@push('scripts')
    <link href="{{asset('css/tasks/overview.css')}}" rel="stylesheet">
@endpush

<x-layout>
    <div id="tasks-overview-main" class="d-flex flex-column justify-content-center align-items-center h-100 w-100">  <!-- Tasks overview main -->
        <form id="tasks-overview-form" class="d-flex flex-column"> <!-- Tasks overview form -->
            @csrf

            <div class="container-fluid">
                <div class="row m-auto ms-5 me-5 mt-5">
                    <div id="left-part" class="col-6">
                        <div class="d-flex flex-column">
                            <div class="mb-4 w-100">
                                    <div id="title-input-container" class="form-group">
                                        <input type="text" class="form-control bg-transparent border-0"  value="{{$task['title']}}" name="title" placeholder="Titlu" readonly>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div id="right-part" class="col-6">
                        <div class="d-flex flex-column">
                            <div class="mb-4 w-100">
                                <div id="course-input-container" class="form-group">
                                    <input type="text" class="form-control bg-transparent border-0" value="{{$task['course']}}" name="course" placeholder="Materia" readonly>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="center-part" class="col-12 d-flex justify-content-center flex-wrap">
                        <div class="d-flex flex-column w-50 d-block mb-4">
                            <div id="deadline-input-container" class="form-group">
                                <input id="taskEditTimePicker" type="text" class="form-control bg-transparent border-0" value="{{$task['deadline']}}" name="deadline" placeholder="Deadline" readonly>
                            </div>
                        </div>
                    
                        <div class="d-flex flex-column w-100">
                            <div id="description-input-container" class="form-group" style="">
                                <textarea type="text" class="form-control bg-transparent border-0" name="description" placeholder="Ce am de facut?" readonly>{{$task['description']}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         
            <div class="form-buttons ms-auto me-3 me-lg-5 pe-lg-3 pb-lg-1 mt-auto mb-4">
                <a id="cancel-button" class="btn btn-primary btn-lg" href="/tasks">Inapoi</a>
            </div>
        </form> <!-- Exams overview form -->
    </div> <!-- Tasks overview main -->
</x-layout>