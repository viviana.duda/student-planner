 @push('scripts')
    <link href="{{ asset('css/tasks/index.css') }}" rel="stylesheet">
    <script src="{{ asset('js/tasks/displayCardMenu.js')}}"></script>
    <script src="{{ asset('js/tasks/serverProgressUpdate.js')}}"></script>
    <script src="{{ asset('js/tasks/clientProgressUpdate.js')}}"></script>
    <script src="{{ asset('js/tasks/deleteFile.js')}}"></script>
    <script src="{{ asset('js/tasks/getFilenameFromContentDisposition.js')}}"></script>
    <script src="{{ asset('js/tasks/downloadFile.js')}}"></script>
    <script src="{{ asset('js/tasks/displayFilesModal.js')}}"></script>
    <script src="{{ asset('js/tasks/displayAckDeleteTask.js')}}"></script>
    <script src="{{ asset('js/tasks/removeTask.js')}}"></script>
    <script src="{{ asset('js/tasks/updateTrackColor.js')}}"></script>
    <script src="{{ asset('js/tasks/showOverview.js')}}"></script>
@endpush 

<x-layout>
    <div class="modal fade remove-task-modal" id="removeTaskModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"> <!-- Remove task modal -->
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content" style="max-width: 400px;">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Esti Sigur?</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
               
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Inchide</button>
                    <button id="remove-task" type="button" class="btn btn-primary" data-bs-dismiss="modal">Sterge</button>
                </div>
            </div>
        </div>
    </div> <!-- Remove task modal -->

    <div class="modal fade task-files-modal" id="displayFilesModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"> <!-- Display task files modal -->
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content" style="height: 500px;">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="staticBackdropLabel">Fisierele Task-ului</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>

                <div id='files-container' class="modal-body">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Inchide</button>
                </div>
            </div>
        </div>
    </div> <!-- Display task files modal -->

    <div id="tasks-main" class="d-flex flex-column w-100 h-100"> <!-- Tasks main -->
        <div id="tasks-main-header" class="d-flex justify-content-start align-items-center ms-5 ps-lg-5 mt-1 mt-xl-5"> <!-- Tasks header -->
            <h2 id="header-title" class="fw-bold">Task-urile tale</h2>
            <a class="btn btn-link mb-2 ps-1 ps-xl-3" href="/task/create"><img id="create-task-icon" src="{{asset('images/plus-sign.svg')}}"></a>
        </div> <!-- Tasks header -->

        <div id="tasks-container" class="container-fluid ms-3 ps-1 mt-3 ms-lg-5 ps-md-5"> <!-- Tasks container -->
            @foreach($tasks as $task)
                <div class="row mt-1 mb-4">
                    <div class="col-9 col-sm-8 col-md-7 col-lg-6 col-xl-6 d-inline-block">
                        <div id="card{{$task->id}}" class="task-card card">
                            <div class="card-body d-flex flex-column ms-3">
                                <div class="d-flex justify-content-between">
                                    <h5 class="card-title mt-2" onclick="showOverview('{{$task->id}}')">{{$task->title}}</h5>
                                    <div onclick="displayCardMenu('{{$task->id}}')">
                                        <img src="{{asset('images/three-dots-icon.svg')}}" class="ms-3 options-box-icon">
                                    </div>    
                                </div> 

                                <div class="progress-container d-flex flex-column mt-2">
                                    <div class="d-flex">
                                        <h6 class="progress-text">Progres</h6>
                                        <h6 class="ms-auto progress-percentage" id="progress{{$task->id}}">{{$task->progress_percentage}}%</h6>
                                    </div>

                                    <input type="range" class="form-range progressRange" min="1" max="100" id="customRange2" value="{{$task->progress_percentage}}" oninput="clientProgressUpdate(this.value, 'progress{{$task->id}}')" onchange="serverProgressUpdate(this.value, 'progress{{$task->id}}', {{$task->id}})">
                                </div>

                                <small class="task-deadline" class="text-nowrap">Data limita {{$task->deadline}}</small>
                            </div>
                        </div>
                    </div>

                    <div class="col-1  flex-column justify-content-center" id="card{{$task->id}}-options-box" style="display: none;"> <!-- Options box container -->
                    </div> <!-- Options box container -->
                </div>
            @endforeach
        </div> <!-- Tasks container -->
    
        <nav id="navigation-container" class="mt-auto align-self-center"> <!-- Tasks navigation footer -->
            <ul class="pagination">
                @if($tasks->onFirstPage())
                    <li class="page-item disabled">
                        <a id="previous-page-button-disabled" class="page-link" href="#" tabindex="-1">Inapoi</a>
                @else
                    <li class="page-item active">
                        <a id="previous-page-button" class="page-link" href="{{$tasks->previousPageUrl()}}" tabindex="-1">Inapoi</a>
                @endif
                    </li>

                <li class="page-item">
                    @if($tasks->nextPageUrl())
                        <a id="next-page-button" class="page-link" href="{{$tasks->nextPageUrl()}}">Urmatoarea</a>
                    @else
                        <a id="next-page-button-disabled" class="page-link disabled" href="{{$tasks->nextPageUrl()}}">Urmatoarea</a>
                    @endif
                </li>
            </ul>
        </nav> <!-- Tasks navigation footer --> 
    </div> <!-- Tasks main -->

    <script> 
        // Update track color //
        const rangeInputElements = document.getElementsByClassName('progressRange');

        for(rangeInputElement of rangeInputElements) {
            rangeInputElement.addEventListener('input', updateTrackColor);
            rangeInputElement.dispatchEvent(new Event('input'));
        }
        //
    </script>
</x-layout>



