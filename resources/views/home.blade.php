@push('scripts')
    <script src="{{asset('js/home/setCalendarItems.js')}}"></script>
    <script src="{{asset('js/home/updateCalendar.js')}}"></script>
    <script src="{{asset('js/home/updateCalendarSchedule.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
@endpush

<x-layout>
    <script>
        var currentDate = dayjs();
        var monthMap = {
            0: 'Jan',
            1: 'Feb',
            2: 'Mar',
            3: 'Apr',
            4: 'May',
            5: 'Jun',
            6: 'Jul',
            7: 'Aug',
            8: 'Sep',
            9: 'Oct',
            10: 'Nov',
            11: 'Dec'
        }
        var calendarData = @json($calendarData);
    </script>
    <div id="home-main" class="d-flex justify-content-center align-items-center h-100 w-100"> <!-- Home Main -->
        <div id="calendar-main" class="d-flex"> <!-- Calendar -->
            <div id="left-panel" class="d-flex flex-column justify-content-between"> <!-- Left Panel -->
                <div id="current-date" class="d-flex mt-4 align-items-center">
                    <div id="day" class="blue-circle ms-1 ms-lg-4">
                        <h1 id="current-date-day"></h1>
                    </div>

                    <div id="month-year" class="flex-shrink-0 ms-2 ms-lg-4 me-2 me-lg-3 mt-4">
                        <h1 id="current-date-month"></h1>
                        <h1 id="current-date-year"></h1>
                    </div>
                </div>

                <div id="calendar-schedule"> 
                    <h3>ORAR</h3>
                    <div id="schedule-items">
                        @for($i = 0; $i < count($calendarData['currentDayScheduleItems']); ++$i)
                            @php $scheduleItem = $calendarData['currentDayScheduleItems'][$i] @endphp
                            <div id="schedule-item-{{$i}}">
                                <h5 id="schedule-item-{{$i}}-interval">{{$scheduleItem['start_hour']}} - {{$scheduleItem['end_hour']}}</h5>
                                <h5 id="schedule-item-{{$i}}-title">{{$scheduleItem['title']}}</h5>
                            </div>
                        @endfor
                        
                        @if(!count($calendarData['currentDayScheduleItems']))
                            <h1 id="no-course-today-message">Nici-un curs astazi</h1>
                        @endif

                    </div>
                </div>
            </div> <!-- Left Panel -->
            
            <div id="right-panel"> <!-- Right Panel -->
                <div id="calendar" class="d-flex flex-wrap h-100 w-100"> <!-- Calendar -->
                    <div id="calendar-header" class="d-flex justify-content-evenly text-center mb-4">
                        <div id="previous">
                            <h1 class="month"></h1>
                            <h1 class="year"></h1>
                        </div>

                        <div id="current">
                            <h1 class="month"></h1>
                            <h1 class="year"></h1>
                        </div>

                        <div id="following">
                            <h1 class="month"></h1>
                            <h1 class="year"></h1>
                        </div>
                    </div>

                    <div id="calendar-body" class="d-flex justify-content-between">
                        <div id="sunday">
                            <h1>SUN</h1>
                        </div>

                        <div id="monday">
                            <h1>MON</h1>
                        </div>
    
                        <div id="tuesday">
                            <h1>TUE</h1>
                        </div>
    
                        <div id="wednesday">
                            <h1>WED</h1>
                        </div>
    
                        <div id="thursday">
                            <h1>THUR</h1>
                        </div>
    
                        <div id="friday">
                            <h1>FRI</h1>
                        </div>
    
                        <div id="saturday">
                            <h1>SAT</h1>
                        </div>
                    </div>
                </div> <!-- Calendar -->
            </div> <!-- Right Panel -->
        </div>
    </div> <!-- Home Main -->
    
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            updateCalendar();
            setCalendarItems();
            updateCalendarSchedule();
        });
    </script>
</x-layout>