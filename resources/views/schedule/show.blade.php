@push('scripts')
    <script src="{{asset('js/schedule/addScheduleItem.js')}}"></script>
    <script src="{{asset('js/schedule/parseFormSubmittedData.js')}}"></script>
    <script src="{{asset('js/schedule/removeScheduleItem.js')}}"></script>
    <script src="{{asset('js/schedule/serverSideScheduleItemModified.js')}}"></script>
    <script src="{{asset('js/schedule/setRemoveFlashMessage.js')}}"></script>
    <link href="{{asset('css/schedule.css')}}" rel="stylesheet">
@endpush

<x-layout>
    <script>
        const scheduleItemsInfoStack = {
            clientSideAdded: [],
            serverSideModified: [],
            serverSideRemoved: [],
            containerAmount: {
                monday: @json(count($scheduleItems['monday'])),
                tuesday: @json(count($scheduleItems['tuesday'])),
                wednesday: @json(count($scheduleItems['wednesday'])),
                thursday: @json(count($scheduleItems['thursday'])),
                friday: @json(count($scheduleItems['friday'])),
                saturday: @json(count($scheduleItems['saturday'])),
                sunday: @json(count($scheduleItems['sunday']))
            }
        };
    </script>

    @if(session('message'))
        <div id="flash-message-schedule-updated">
            <h3>{{ session('message') }}</h3>
        </div>

        <script>
            setRemoveFlashMessage();
        </script>
    @endif
    
    <form id="schedule-main" class="mt-5 d-flex justify-content-evenly" action="/schedule" method="POST" onsubmit="parseFormSubmittedData(event, scheduleItemsInfoStack)">
        @csrf
        @method('PUT')

        @php 
            $days = [ 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ];
            $daysRoTranslate = [
                'monday' => 'luni',
                'tuesday' => 'marti', 
                'wednesday' => 'miercuri',
                'thursday' => 'joi', 
                'friday' => 'vineri', 
                'saturday' => 'sambata', 
                'sunday' => 'duminica'
            ];
        @endphp

        @foreach($days as $day)
            <div id="{{$day}}" class="d-flex flex-column text-center">
                <h1 class="schedule-day-title">{{ucfirst($daysRoTranslate[$day])}}</h1>
                @if(count($scheduleItems[$day]))
                    @for($i = 0; $i < count($scheduleItems[$day]); ++$i)
                        @php $scheduleItemId = $scheduleItems[$day][$i]['id']; @endphp

                        <div class="schedule-item" id="{{$day}}-schedule-item-{{$scheduleItemId}}">
                            <img class="remove-schedule-item" onclick="removeScheduleItem('{{$day}}-schedule-item-{{$scheduleItemId}}', scheduleItemsInfoStack)" src="{{asset('images/xmark-icon.svg')}}">
                            
                            <div class="title">
                                <input type="text" value="{{$scheduleItems[$day][$i]['title']}}" name="{{$day}}-schedule-item-{{$scheduleItemId}}-title" onchange="serverSideScheduleItemModified('{{$day}}-schedule-item-{{$scheduleItemId}}', scheduleItemsInfoStack)">
                            </div>
                            
                            <div class="time-interval">
                                <img class="schedule-item-clock-icon" src="{{asset('images/dark-clock-icon.svg')}}">

                                <input class="schedule-item-start-time" type="time" value="{{$scheduleItems[$day][$i]['start_hour']}}" name="{{$day}}-schedule-item-{{$scheduleItemId}}-start-hour" value="00:00" onchange="serverSideScheduleItemModified('{{$day}}-schedule-item-{{$scheduleItemId}}', scheduleItemsInfoStack)"> 
                                <small>-&nbsp;</small>
                                <input class="schedule-item-end-time" type="time" value="{{$scheduleItems[$day][$i]['end_hour']}}" name="{{$day}}-schedule-item-{{$scheduleItemId}}-end-hour" value="00:00" onchange="serverSideScheduleItemModified('{{$day}}-schedule-item-{{$scheduleItemId}}', scheduleItemsInfoStack)">
                            </div>
                        </div>
                    @endfor
                @endif
                <img class="add-schedule-item" src="{{asset('images/plus-sign.svg')}}" onclick="addScheduleItem('{{$day}}', scheduleItemsInfoStack)">
            </div>
        @endforeach
        
        <button id="save-schedule-button" class="btn btn-primary" type="submit" value="Salveaza"><img src="{{asset('images/check-icon.svg')}}"></button>
    </form>
</x-layout>