@php 
    $todayEvents = $layoutData['todayEvents'];
    $tommorowEvents = $layoutData['tommorowEvents'];
    $userProfileImagePath = $layoutData['userProfileImagePath']
@endphp
  
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>My student planner</title>
        
        {{-- Bootstrap --}}
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <script src="{{asset('js/bootstrap.min.js')}}"></script>

        <!-- Dayjs -->
        <script src="{{asset('js/dayjs.min.js')}}"></script>
        <script src="{{asset('js/timezone.min.js')}}"></script>

        {{-- Flat Pickr --}}
        <link href="{{asset('css/flatpickr.min.css')}}" rel="stylesheet">
        <script src="{{asset('js/flatpickr.js')}}" crossorigin="anonymous"></script>

        {{-- JQuery --}}
        <script src="{{asset('js/jquery-3.6.0.min.js')}}"></script>

        {{-- Custom CSS --}}
        <link href="{{asset('css/layout.css')}}" rel="stylesheet">

        {{-- Custom JS --}}
        <script src="{{asset('js/layout/convertTo24HourFormat.js')}}"></script>
        <script src="{{asset('js/layout/sendAlertsAck.js')}}"></script>
        <script src="{{asset('js/layout/displayNotifications.js')}}"></script>
        <script src="{{asset('js/layout/getNextTimeUpdateTimestamp.js')}}"></script>
        <script src="{{asset('js/layout/remaindAt.js')}}" ></script>
        <script src="{{asset('js/layout/setNotifications.js')}}" defer></script>
        <script src="{{asset('js/layout/updateTodayEvents.js')}}" defer></script>
        <script src="{{asset('js/layout/setActiveNavLink.js')}}"></script>

        {{-- Childs scripts --}}
        @stack('scripts')

        <script>
            var events = {
                todayEvents: @json($todayEvents),
                tommorowEvents: @json($tommorowEvents)
            };

            var alerts = @json(auth()->user()->notifications());
            var activeAlerts = {
                count: 0,
                alerts: []
            };
        </script>
     </head>
    <body>
        <div class="modal fade" id="notifications-modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"> <!-- Notifications Modal --> 
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-sm">
                <div class="modal-content" style="max-width: 400px;">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="staticBackdropLabel">Notificari</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                
                    <div class="modal-body text-center">
                        <ul id="notifications-list" class="list-group list-group-flush">
                        </ul>
                    </div>
                </div>
            </div>
        </div> <!-- Notifications modal -->

        <div id="page" class="container-fluid"><!-- Page -->
            <div id="page-container" class="row"> <!-- Page container -->
                
                <div id="sidebar" class="d-flex flex-column col-1 align-items-center"> <!-- Sidebar -->
                    <a id="home-page-link" class="mt-3 mt-sm-3 mt-md-3 mt-lg-4 mt-xl-5" href="/"><img src="{{asset('images/home-icon.svg')}}"></a>
                    <a id="tasks-page-link" class="mt-3 mt-sm-3 mt-md-3 mt-lg-4 mt-xl-5" href="/tasks"><img src="{{asset('images/tasks-icon.svg')}}"></a>
                    <a id="calendar-page-link" class="mt-3 mt-sm-3 mt-md-3 mt-lg-4 mt-xl-5" href="/calendar"><img src="{{asset('images/calendar-icon.svg')}}"></a>
                    <a id="exams-page-link" class="mt-3 mt-sm-3 mt-md-3 mt-lg-4 mt-xl-5" href="/exams"><img src="{{asset('images/exams-icon.svg')}}"></a>
                    <a id="schedule-page-link" class="mt-3 mt-sm-3 mt-md-3 mt-lg-4 mt-xl-5" href="/schedule"><img src="{{asset('images/schedule-icon.svg')}}"></a>
                    <a id="dashboard-page-link" class="mt-3 mt-sm-3 mt-md-3 mt-lg-4 mt-xl-5" href="/dashboard"><img src="{{asset('images/dashboard-icon.svg')}}"></a>
                    <a class="mt-auto mb-1 mb-sm-1 mb-md-2 mb-lg-3 mb-xl-4 mb-xxl-5" href="/logout"><img src="{{asset('images/exit-icon.svg')}}"></a>
                </div> <!-- Sidebar -->
    
                <div id="main-content" class="d-flex flex-column col-11 col-md-9 nowrap"> <!-- Main content -->
                    <div id="header" class="pt-3 d-flex justify-content-between align-items-center"> <!-- Header -->
                        <h1 id="header-title" class="ms-0 ms-lg-4 text-nowrap"><b>My Planner</b></h1>

                        <div id="notifications-image-container" class="me-lg-4 d-flex justify-content-between align-items-center">
                            <div id="notifications" class="d-inline d-flex me-sm-1 me-md-2 pt-0 align-self-center">
                                <button id='notifications-button' onclick="displayNotifications()"><img src="{{asset('images/notification-bell.svg')}}"></button>
                                <span id="notifications-count" class="align-self-start" class="badge badge-light">0</span>
                            </div>
                            
                            @if(isset($userProfileImagePath))
                                <a id="avatar" class="d-inline" href="/profile">
                                    <img src="{{ Storage::url($userProfileImagePath) }}?timestamp={{ time() }}">
                                </a>
                            @else 
                                <a id="avatar" class="d-inline" href="/profile">
                                    <img src="{{ asset('images/default-avatar.png') }}?timestamp={{ time() }}">
                                </a>
                            @endif
                        </div>
                    </div>  <!-- Header -->                 
    
                    <div id="main" class="flex-grow-1"> <!-- Childs Main -->
                        {{$slot}}   
                    </div> <!-- Childs Main -->

                </div> <!-- Main content -->
            
                <div id="activity-bar" class="d-flex flex-column col-2 d-sm-none d-md-flex align-items-center"> <!-- Activity bar -->
                    <div id="activity-bar-title" class="mt-5 text-center mb-3"> <!-- Activites message -->
                        <h4>Activități</h4>
                    </div> <!-- Activites message -->
                    
                    <div id="current-day"> <!-- Current Day -->
                        @for($i = 0; $i < count($todayEvents) && $i <= 1; ++$i)
                            <div class="card mt-3 today-card" style="background: linear-gradient(to bottom, {{$todayEvents[$i]['calendar_color'] ?: '#dfa67b'}},  #fff);">
                                <div class="card-body d-flex flex-column">
                                    <h5 class="card-title">{{$todayEvents[$i]['title']}}</h5>
                                    <h6 class="card-subtitle mb-2">{{$todayEvents[$i]['subtitle']}}</h6>
    
                                    @if(isset($todayEvents[$i]['location'])) 
                                        <div>
                                            <img class="card-location-icon" src="{{asset('images/location-pin.svg')}}">&nbsp;
                                            <span class="card-location">{{$todayEvents[$i]['location']}}</span>  
                                        </div>
                                    @else
                                        <br>
                                    @endif
                                    
                                    <div class="mt-1">
                                        @php
                                            $eventPeriod = $todayEvents[$i]['period'];
                                            $startTime = null;
                                            $endTime = null;

                                            if(isset($eventPeriod['start_time'])) 
                                                $startTime = $eventPeriod['start_time'];

                                            if(isset($eventPeriod['end_time'])) 
                                                $endTime = $eventPeriod['end_time'];

                                            if(strlen($startTime) && strlen($endTime))
                                                $endTime = ' - ' . $endTime;
                                        @endphp
    
                                        <img class="card-clock-icon" src="{{asset('images/clock-icon.svg')}}">

                                        @if(isset($startTime))
                                            <div class="ms-2 mb-1 event-start-time d-inline">
                                                <small>{{$startTime}}</small>
                                            </div>
                                        @endif

                                        @if(isset($endTime))
                                            @if(isset($startTime))
                                                <div class="event-end-time d-inline">
                                            @else
                                                <div class="ms-2 event-start-time d-inline">
                                            @endif
                                                <small>{{$endTime}}</small>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endfor

                        @if(!count($todayEvents))
                            <div class="today-card">
                            </div>
                            <div class="today-card">
                            </div>
                        @endif
                    </div> <!-- Current Day -->
    
                    <div id="next-day" class="mt-4"> <!-- Next Day -->
                        <div id="next-day-bar-title" class="text-center">
                            <h4 class="mt-4">Următoarea zi</h4>
                        </div>
    
                        @php 
                            $firstActive = true;
                        @endphp
    
                        @if(count($tommorowEvents))
                            <div id="activityCarousel" class="carousel slide mt-3 d-flex flex-column align-items-center" data-bs-ride="false">
                                <div class="carousel-inner">
                                    @foreach ($tommorowEvents as $tommorowEvent)
                                        @if($firstActive)
                                            <div class="carousel-item active">
                                        @else
                                            <div class="carousel-item">
                                        @endif
                                                <div class="next-day-card" class="card" style="background: linear-gradient(to bottom,  {{$tommorowEvent['calendar_color'] ?: '#dfa67b'}},  #fff);">
                                                    <div class="card-body d-flex flex-column">
                                                        <h5 class="card-title">{{$tommorowEvent['title']}}</h5>
                                                        <h6 class="card-subtitle mb-2">{{$tommorowEvent['subtitle']}}</h6>
                            
                                                        @if(isset($tommorowEvent['location'])) 
                                                            <div>
                                                                <img class="card-location-icon" src="{{asset('images/location-pin.svg')}}">&nbsp;
                                                                <span class="card-location">{{$tommorowEvent['location']}}</span>  
                                                            </div>
                                                        @else
                                                            <br>
                                                        @endif
                                                    
                                                        <div class="mt-1">
                                                            @php
                                                                $eventPeriod = $tommorowEvent['period'];
                                                                $startTime = null;
                                                                $endTime = null;
                    
                                                                if(isset($eventPeriod['start_time'])) 
                                                                    $startTime = $eventPeriod['start_time'];
                
                                                                if(isset($eventPeriod['end_time'])) 
                                                                    $endTime = $eventPeriod['end_time'];
                
                                                                if(strlen($startTime) && strlen($endTime))
                                                                    $endTime = ' - ' . $endTime;
                                                            @endphp
                        
                                                            <img class="card-clock-icon" src="{{asset('images/clock-icon.svg')}}">
                    
                                                            @if(isset($startTime))
                                                                <div class="ms-2 mb-1 event-start-time d-inline">
                                                                    <small>{{$startTime}}</small>
                                                                </div>
                                                            @endif
                    
                                                            @if(isset($endTime))
                                                                @if(isset($startTime))
                                                                    <div class="event-end-time d-inline">
                                                                @else
                                                                    <div class="ms-2 event-start-time d-inline">
                                                                @endif
                                                                    <small>{{$endTime}}</small>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @php $firstActive = false @endphp
                                    @endforeach
                                </div>

                                <button class="carousel-control-next mt-sm-2 mt-md-4" type="button" data-bs-target="#activityCarousel" data-bs-slide="next">
                                    <img id="arrow-button" src="{{asset('images/arrow-down-icon.svg')}}" >
                                </button>
                            </div>
                        @else
                            <h1 id="no-next-day-event-message" class="text-center pt-3">Nici-un eveniment</h1>
                        @endif
                    </div> <!-- Next Day -->
                </div> <!-- Activity bar -->
            </div> <!-- Page container -->
        </div> <!-- Page -->
    </body>
    
    <script> 
        document.addEventListener('DOMContentLoaded', function() {
            setActiveNavLink();
            setNotifications();
            updateTodayEvents();
        });
    </script>
</html>
 