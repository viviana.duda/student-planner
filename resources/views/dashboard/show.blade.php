@push('scripts')
   <script src="{{ asset('js/chart.js') }}"></script>
   <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
@endpush

<x-layout>
 
    <div id="chartContainer">
        <canvas id="lineChart"></canvas>
    </div>

    <script>
        var data = {
            labels: ['Luni', 'Marti', 'Miercuri', 'Joi', 'Vineri', 'Sambata', 'Duminica'],
            datasets: [
              {
                label: 'Task-uri create',
                data: @json($statistics['createdTasks']),
                borderColor: 'black',
              },
            {
                label: 'Examene create',
                data: @json($statistics['createdExams']),
                borderColor: 'red',
            },
            {
                label: 'Examene promovate',
                data: @json($statistics['passedExams']),
                borderColor: 'green',
            },
            {
                label: 'Task-uri terminate',
                data: @json($statistics['finishedTasks']),
                borderColor: 'orange',
            },
          ]
        };

        var options = {
            responsive: true,
            maintainAspectRatio: false
        };

        var ctx = document.getElementById('lineChart').getContext('2d');
        new Chart(ctx, {
            type: 'line',
            data: data,
            options: options
        });
    </script>
</x-layout>