<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('course_name', 60)->nullable(false);
            $table->string('class_name', 60)->nullable(false);
            $table->string('teacher_name', 50)->nullable(false);
            $table->boolean('alert_ack')->default(false);
            $table->dateTime('date')->nullable(false);
            $table->integer('duration')->nullable(false);
            $table->string('calendar_color', 7)->default('#0000FF');
            $table->string('box_color', 7)->default('#00FF00');
            $table->boolean('passed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('exams');
    }
};
