<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table('faculties')->insert([
            ['name' => 'Universitatea Bucuresti'],
            ['name' => 'Universitatea Alexandru Ioan Cuza'],
            ['name' => 'Universitatea Babes-Bolyai'],
            ['name' => 'Universitatea de Vest din Timisoara'],
            ['name' => 'Universitatea din Craiova'],
            ['name' => 'Universitatea de Medicina si Farmacie Carol Davila'],
            ['name' => 'Universitatea Politehnica din Bucuresti'],
            ['name' => 'Universitatea Tehnica Cluj-Napoca'],
            ['name' => 'Universitatea de Medicina si Farmacie Iuliu Hatieganu'],
            ['name' => 'Universitatea Ovidius Constanta'],
            ['name' => 'Universitatea Transilvania Brasov'],
            ['name' => 'Universitatea de Nord Baia Mare'],
            ['name' => 'Universitatea din Oradea'],
            ['name' => 'Universitatea de Stiinte Agricole si Medicina Veterinara Cluj-Napoca'],
            ['name' => 'Universitatea din Pitesti'],
            ['name' => 'Universitatea din Sibiu'],
            ['name' => 'Universitatea de Arhitectura si Urbanism Ion Mincu'],
            ['name' => 'Universitatea din Ploiesti'],
            ['name' => 'Universitatea Politehnica Timisoara'],
            ['name' => 'Universitatea de Vest Vasile Goldis'],
            ['name' => 'Universitatea de Medicina si Farmacie Grigore T. Popa Iasi'],
            ['name' => 'Universitatea de Arhitectura si Urbanism din Bucuresti'],
            ['name' => 'Universitatea de Arte din Bucuresti'],
            ['name' => 'Universitatea de Muzica si Arta Dramatica Gheorghe Dima'],
            ['name' => 'Universitatea de Stiinte Agronomice si Medicina Veterinara a Banatului'],
            ['name' => 'Universitatea de Medicina, Farmacie, Stiinte si Tehnologie din Targu Mures'],
            ['name' => 'Universitatea de Arte George Enescu Iasi'],
            ['name' => 'Universitatea de Stiinte Agronomice si Medicina Veterinara a Banatului din Timisoara'],
            ['name' => 'Universitatea de Medicina si Farmacie Victor Babes Timisoara'],
            ['name' => 'Universitatea Hyperion din Bucuresti'],
            ['name' => 'Universitatea de Medicina, Farmacie, Stiinte si Tehnologie din Oradea'],
            ['name' => 'Universitatea de Stiinte Agronomice si Medicina Veterinara a Cluj-Napoca'],
            ['name' => 'Universitatea Crestina Dimitrie Cantemir'],
            ['name' => 'Universitatea din Bacau'],
            ['name' => 'Universitatea de Medicina si Farmacie din Timisoara'],
            ['name' => 'Universitatea de Medicina, Farmacie, Stiinte si Tehnologie din Craiova'],
            ['name' => 'Universitatea de Medicina si Farmacie din Targu Mures'],
            ['name' => 'Universitatea de Medicina, Farmacie, Stiinte si Tehnologie din Suceava'],
            ['name' => 'Universitatea de Medicina, Farmacie, Stiinte si Tehnologie din Brasov'],
            ['name' => 'Universitatea Tehnica Gheorghe Asachi din Iasi'],
            ['name' => 'Universitatea Tehnica din Moldova'],
            ['name' => 'Universitatea de Arhitectura si Urbanism Ion Mincu din Bucuresti'],
            ['name' => 'Universitatea de Arte din Targu Mures'],
            ['name' => 'Universitatea de Stiinte Agricole si Medicina Veterinara Iasi'],
            ['name' => 'Universitatea de Stiinte Agricole si Medicina Veterinara Bucuresti'],
            ['name' => 'Universitatea de Stiinte Agronomice si Medicina Veterinara a Banatului Timisoara'],
            ['name' => 'Universitatea de Stiinte Agronomice si Medicina Veterinara a Banatului din Arad']
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
