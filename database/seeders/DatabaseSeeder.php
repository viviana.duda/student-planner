<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Exam;
use App\Models\Task;
use App\Models\User;
use App\Models\Faculty;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        User::factory(6)->create();
        for($i = 1; $i <= 5; ++$i){
            Task::create([
                "user_id" => 6,
                "title" => "Titlu $i",
                "course" => "Course $i",
                "deadline" => "2023-05-1 20:00:00",
                "description" => "$i",
                "progress_percentage" => 11
            ]);

            Exam::create([
                "user_id" => $i,
                "course_name" => "Course Name $i",
                "class_name" => "Class name $i",
                "teacher_name" => "Teacher $i",
                "date" => "2023-05-1 20:05:05",
                "duration" => 10+$i,
                "passed" => ($i % 2 == 0 ? true : false)
            ]);
        };
    }
}
