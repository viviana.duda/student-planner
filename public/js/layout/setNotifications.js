function setNotifications() {
    if(alerts && alerts.length) {
        for(let index in alerts) {
            if(!alerts[index])
                continue;

            if((alerts[index].remainedAt * 1000) < dayjs().valueOf()) {
                delete alerts[index];
                continue;
            }

            setTimeout(() => {
                ++activeAlerts.count;
                activeAlerts.alerts.push(alerts[index]);

                const notificationsCount = document.getElementById('notifications-count');
                notificationsCount.innerHTML = `${activeAlerts.count}`;

                delete alerts[index];
            }, remaindAt(alerts[index].remainedAt * 1000))
        }
    }
}