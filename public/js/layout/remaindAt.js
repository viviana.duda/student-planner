function remaindAt(eventTimestamp) {
    const twoHoursTimestamp = 7200000;

    if(!((eventTimestamp - dayjs().valueOf()) > twoHoursTimestamp))  
        return 0;

    let remaindAt = (eventTimestamp - twoHoursTimestamp) - dayjs().valueOf();

    return remaindAt > 0 ? remaindAt : 0;
}