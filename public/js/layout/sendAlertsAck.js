 function sendAlertsAck() {
    fetch('/notifications/acknowledge', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
        body: JSON.stringify(activeAlerts.alerts)
    });   
}