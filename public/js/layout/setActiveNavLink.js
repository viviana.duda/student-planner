function setActiveNavLink() {
    const iconIdToPageMap = {
        "": "home-page-link",
        "tasks": "tasks-page-link",
        "task": "tasks-page-link",
        "calendar": "calendar-page-link",
        "exams": "exams-page-link",
        "exam": "exams-page-link",
        "schedule": "schedule-page-link",
        "dashboard": "dashboard-page-link",
    }        

    let currentPage = new URL(window.location.href).pathname.split('/')[1];
    console.log(currentPage);
    const navElement = document.getElementById(iconIdToPageMap[currentPage]);

    navElement.style.boxShadow = "0px 5px 0px 0px orange";
    navElement.style.transform = 'translateY(-5px)';
}