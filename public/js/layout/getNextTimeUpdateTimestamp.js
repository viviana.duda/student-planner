
function getNextTimeUpdateTimestamp(events) {
    let firstEvent = events[0];

    let splittedHour = firstEvent.period.start_time || firstEvent.period.end_time;
    let parsedHour = convertTo24HourFormat(splittedHour).split(':');

    let eventEndHour = parsedHour[0];
    let eventEndHourMinutes = parsedHour[1];

    let currentDateTime = dayjs();
    let nextTimeUpdate = currentDateTime.set('hour', eventEndHour).set('minute', eventEndHourMinutes).valueOf() - dayjs().valueOf();

    if(nextTimeUpdate <= 0) 
        nextTimeUpdate = 60000;

    return nextTimeUpdate;
}