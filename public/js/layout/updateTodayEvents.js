
 function updateTodayEvents() {
    if(!events.todayEvents.length) 
        return;

    setTimeout(() => {
        let todayEventsContainer = document.getElementById('current-day');
        todayEventsContainer.innerHTML = '';

        fetch('/user/getEvents', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
            body: JSON.stringify({
                eventsPeriod: 'today'
            })
        })
            .then((response) => {
                response.json().then((data) => {
                    let todayEvents = data;
    
                    if(!todayEvents.length) { // temporarily for testing
                        const spaceCard1 = document.createElement('div');
                        spaceCard1.className = 'today-card';

                        const spaceCard2 = document.createElement('div');
                        spaceCard2.className = 'today-card';

                        todayEventsContainer.appendChild(spaceCard1);
                        todayEventsContainer.appendChild(spaceCard2);
                        
                        return;
                    }

                    if(todayEvents.length) {
                        for(i = 0; i <= 1 && i < todayEvents.length; ++i){
                            const todayEventCard = document.createElement('div');
                            todayEventCard.className = 'card mt-3 today-card';
                            todayEventCard.style.background = `linear-gradient(to bottom, ${todayEvents[i]['calendar_color'] || '#dfa67b'},  #fff)`;

                            let eventPeriod = todayEvents[i]['period'];
                            let startTime = null;
                            let endTime = null;
            
                            if(eventPeriod['start_time']) 
                                startTime = eventPeriod['start_time'];
            
                            if(eventPeriod['end_time'])
                                endTime = eventPeriod['end_time'];
                
                            if(startTime && endTime)
                                endTime = ' - ' + endTime;
                            
                            let currentUrl = window.location.origin;

                            todayEventCard.innerHTML = `                            
                                <div class="card-body d-flex flex-column">
                                    <h5 class="card-title">${todayEvents[i]['title']}</h5>
                                    <h6 class="card-subtitle mb-2">${todayEvents[i]['subtitle']}</h6>

                                    ${todayEvents[i]['location'] ? 
                                        `
                                            <div>
                                                <img class="card-location-icon" src="${currentUrl}/images/location-pin.svg">&nbsp;
                                                <span class="card-location">${todayEvents[i]['location']}</span>  
                                            </div>  
                                        `
                                        : ''
                                    } 

                                    <div class="mt-1">
                                        <img class="card-clock-icon" src="${currentUrl}/images/clock-icon.svg">
                                        ${startTime ? 
                                            `
                                                <div class="ms-2 mb-1 event-start-time d-inline">
                                                    <small>${startTime}</small>
                                                </div>
                                            ` : ''
                                        }
                                
                                        ${endTime && startTime ? '<div class="event-end-time d-md-none d-lg-inline">' : ''}
                                        ${endTime && !startTime ? '<div class="ms-2 event-end-time d-lg-inline">' : ''} 
                                        <small>${endTime}</small>
                                        </div>
                                    </div>
                                </div>
                            `;

                            todayEventsContainer.appendChild(todayEventCard);
                        }

                        events.todayEvents = todayEvents;
                        setTimeout(updateTodayEvents, getNextTimeUpdateTimestamp(todayEvents));
                }});
            })
            .catch((error) => {   
                setTimeout(updateTodayEvents, 60000);
            });

    }, getNextTimeUpdateTimestamp(events.todayEvents))
}