 function convertTo24HourFormat(time) {
    const [formattedTime, period] = time.split(' ');

    let [hour, minute] = formattedTime.split(':');

    hour = parseInt(hour);

    if (period === 'PM' && hour !== 12) {
        hour += 12;
    } else if (period === 'AM' && hour === 12) {
        hour = 0;
    }

    hour = hour.toString().padStart(2, '0');

    return `${hour}:${minute}`;
}