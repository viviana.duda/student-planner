
 function displayNotifications() {
    let notificationsModal = document.getElementById('notifications-modal')

    if(activeAlerts.count) {
        notificationsModal.setAttribute('data-bs-toggle', 'modal');
        notificationsModal.setAttribute('data-bs-target', '#notifications-modal');
        notificationsModal.click();

        const notificationsButton = document.getElementById('notifications-button');
        const notificationsCount = document.getElementById('notifications-count');
        const notificationsList = document.getElementById('notifications-list');

        notificationsModal = document.createElement('div');

        let notificationElements = activeAlerts.alerts.map((alert) => {
            if(alert.eventType == 'Task')
                return `<li class="notification-message list-group-item"> <b>Task</b> <br> ${alert.title} <br> Ora ${alert.dueDate}</li>`;
            else if(alert.eventType == 'Course')
                return `<li class="notification-message list-group-item"> <b>Curs</b> <br> ${alert.title} <br> Ora ${alert.dueDate}</li>`;    
            
            return `<li class="notification-message list-group-item"> <b>Examen</b> <br> ${alert.title} <br> Ora ${alert.dueDate}</li>`;
        })

        notificationsList.innerHTML = notificationElements.join(' ');
        
        notificationsButton.style.color = "#676a6f";

        sendAlertsAck();
        notificationsCount.innerHTML = '0';
        activeAlerts.alerts = []; 
        activeAlerts.count = 0;
    }
}