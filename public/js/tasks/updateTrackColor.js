function updateTrackColor(rangeInput) {
    rangeInput = rangeInput.target;
    const minValue = parseInt(rangeInput.min);
    const maxValue = parseInt(rangeInput.max);
    const value = parseInt(rangeInput.value);
    
    rangeInput.style.setProperty('--min-value', minValue);
    rangeInput.style.setProperty('--max-value', maxValue);
    rangeInput.style.setProperty('--value', value);
    
    if (value === 100) {
        rangeInput.style.setProperty('--track-color', 'green');
    } else {
        rangeInput.style.setProperty('--track-color', '#5f2b19');
    }
}
