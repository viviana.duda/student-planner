function getFilenameFromContentDisposition(contentDisposition) {
    let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    let matches = filenameRegex.exec(contentDisposition);
    if (matches !== null && matches[1]) {
        return matches[1].replace(/['"]/g, '');
    }
    return null;
}