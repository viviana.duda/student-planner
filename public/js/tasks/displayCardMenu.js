function displayCardMenu(elementId) {
    cardId = elementId;

    const cardOptionsBoxContainer = document.getElementById(`card${cardId}-options-box`);
    const currentUrl = window.location.origin;

    if(window.getComputedStyle(cardOptionsBoxContainer).display === 'none') {
        cardOptionsBoxContainer.style.display = 'flex';
        
        const optionsBoxExists = cardOptionsBoxContainer.childElementCount;
        
        if(optionsBoxExists)
            return;

        const optionsBox = document.createElement('div');
        const removeTask = document.createElement('button');
        const modifyTask = document.createElement('a');
        const viewFiles = document.createElement('button');

        const modifyTaskImage = document.createElement('img');
        modifyTaskImage.src = `${currentUrl}/images/modify-item.svg`;
        modifyTask.appendChild(modifyTaskImage);
        modifyTask.href = `/task/${cardId}/edit`;

        viewFiles.style.border = '0';
        viewFiles.style.background = `url(${currentUrl}/images/view-files.svg) no-repeat`;
        viewFiles.style.backgroundSize = 'contain';
        viewFiles.style.height = '3vmin';
        viewFiles.style.marginBottom = '1.9vmin';
        viewFiles.style.width = '3vmin';

        removeTask.style.border = '0';
        removeTask.style.background = `url(${currentUrl}/images/remove-item.svg) no-repeat`;
        removeTask.style.backgroundSize = 'contain';
        removeTask.style.height = '3vmin';
        removeTask.style.width = '3vmin';

        removeTask.setAttribute('data-bs-toggle', 'modal');
        removeTask.setAttribute('data-bs-target', '#removeTaskModal');
        removeTask.addEventListener('click', displayAckDeleteTask.bind(null, cardId));
        
        viewFiles.setAttribute('data-bs-toggle', 'modal');
        viewFiles.setAttribute('data-bs-target', '#displayFilesModal');
        viewFiles.addEventListener('click', displayFilesModal.bind(null, cardId));

        modifyTaskImage.style.height = "3.3vmin";
        modifyTaskImage.style.width = "3.3vmin";
        modifyTask.style.marginBottom = "1.5vmin";

        // Box Element
        optionsBox.style.display = 'flex';
        optionsBox.style.flexDirection = 'column';
        optionsBox.style.justifyContent = 'between';
        optionsBox.style.alignItems = 'center';
        //

        optionsBox.appendChild(modifyTask);
        optionsBox.appendChild(viewFiles);
        optionsBox.appendChild(removeTask);

        cardOptionsBoxContainer.appendChild(optionsBox);
    }else {
        cardOptionsBoxContainer.style.display = 'none';
    }
}