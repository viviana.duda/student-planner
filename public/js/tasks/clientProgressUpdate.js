function clientProgressUpdate(newValue, rangeProgressDisplayElementId){
    let displayProgressElement = document.getElementById(rangeProgressDisplayElementId);
    displayProgressElement.textContent = newValue + '%';
}