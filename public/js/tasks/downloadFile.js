function downloadFile(taskId, fileId) {
    fetch(`/task/${taskId}/downloadFile/${fileId}`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    })
        .then(response => {
            const blobPromise = response.blob();
            return Promise.all([response, blobPromise]);
        })
        .then(([response, blob]) => {
            const dispositionHeader = response.headers.get('content-disposition');
            const filename = getFilenameFromContentDisposition(dispositionHeader);

            const link = document.createElement('a');
            const url = URL.createObjectURL(blob);
            link.href = url;
            link.download = filename || 'file.ext';

            link.dispatchEvent(new MouseEvent('click'));

            URL.revokeObjectURL(url);
        })
}