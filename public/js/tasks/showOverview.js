function showOverview(taskId) {
    window.location.href = `/task/${taskId}/overview`;
}