function removeTask(taskId) {
    fetch(`/task/${taskId}/remove`, {
        method: 'GET',
        redirect: 'follow', 
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    }).then((response) => {
        window.location.href = '/tasks';
    })
}