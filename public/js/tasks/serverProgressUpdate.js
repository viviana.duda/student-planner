function serverProgressUpdate(newValue, rangeProgressDisplayElementId, taskId) {
    let displayProgressElement = document.getElementById(rangeProgressDisplayElementId);
    displayProgressElement.textContent = newValue + '%';

    fetch(`/task/${taskId}/progress`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
        body: JSON.stringify({
            id: taskId,
            progress_percentage: newValue
        })
    });    
}
