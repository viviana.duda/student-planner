function displayFilesModal(taskId){
    const filesModalBody = document.getElementById('files-container');
    filesModalBody.innerHTML = '';

    fetch(`/task/${taskId}/files`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    }).then(response => {
        response.json().then(files => {
            for(let file of files) {
                const fileElement = document.createElement('div');
                fileElement.style.display = 'inline-block';
                fileElement.style.textAlign = 'center';

                fileElement.innerHTML = `
                    <div class="file-icon">
                    </div>

                    <button class="download-file-button" onclick="downloadFile('${taskId}', '${file.fileId}')"></button>
                    <button class="remove-file-button"onclick="deleteFile('${taskId}','${file.fileId}')"></button>

                    <h6 style="margin-bottom: 50px;">${file.fileName}</h6>
                `;

                filesModalBody.appendChild(fileElement);
             }
        });
    });
}