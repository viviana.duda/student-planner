function deleteFile(taskId, fileId) {
    fetch(`/task/${taskId}/removeFile/${fileId}`, {
        method: 'DELETE',
        redirect: 'follow',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    }).then((response) => {
        window.location.href = '/tasks'
    })
}