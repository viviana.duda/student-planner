function displayAckDeleteTask(taskId){
    const deleteTaskButton = document.getElementById('remove-task');
    deleteTaskButton.onclick = function() { removeTask(taskId); };
}