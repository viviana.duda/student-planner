function displayCardMenu(elementId) {
    cardId = elementId;

    const cardOptionsBoxContainer = document.getElementById(`card${cardId}-options-box`);
    const currentUrl = window.location.origin;

    if(window.getComputedStyle(cardOptionsBoxContainer).display === 'none') {
        cardOptionsBoxContainer.style.display = 'flex';
        
        const optionsBoxExists = cardOptionsBoxContainer.childElementCount;
        
        if(optionsBoxExists)
            return;

        const optionsBox = document.createElement('div');
        const passedExamChecker = document.createElement('button');
        const removeExam = document.createElement('button');
        const modifyExam = document.createElement('a');

        const modifyExamImage = document.createElement('img');
        modifyExamImage.src = `${currentUrl}/images/modify-item.svg`;
        modifyExam.appendChild(modifyExamImage);
        modifyExam.href = `/exam/${cardId}/edit`;
        modifyExamImage.style.height = "3.3vmin";
        modifyExamImage.style.width = "3.3vmin";
        modifyExam.style.marginBottom = "1.9vmin";

        removeExam.style.border = '0';
        removeExam.style.background = `url(${currentUrl}/images/remove-item.svg) no-repeat`;
        removeExam.style.backgroundSize = 'contain';
        removeExam.style.height = '3vmin';
        removeExam.style.width = '3vmin';

        passedExamChecker.style.border = '0';
        passedExamChecker.style.background = `url(${currentUrl}/images/check-exam.svg) no-repeat`;
        passedExamChecker.style.backgroundSize = 'contain';
        passedExamChecker.style.height = '2.7vmin';
        passedExamChecker.style.width = '2.7vmin';
        passedExamChecker.style.marginBottom = '1.9vmin';

        removeExam.setAttribute('data-bs-toggle', 'modal');
        removeExam.setAttribute('data-bs-target', '#removeExamModal');
        removeExam.addEventListener('click', ackDeleteExam.bind(null, cardId));
        
        passedExamChecker.setAttribute('data-bs-toggle', 'modal');
        passedExamChecker.setAttribute('data-bs-target', '#examPassedModal');
        passedExamChecker.addEventListener('click', ackExamPassed.bind(null, cardId));

        // Box Element
        optionsBox.style.display = 'flex';
        optionsBox.style.flexDirection = 'column';
        optionsBox.style.justifyContent = 'between';
        optionsBox.style.alignItems = 'center';
        //

        optionsBox.appendChild(passedExamChecker);
        optionsBox.appendChild(modifyExam);
        optionsBox.appendChild(removeExam);

        cardOptionsBoxContainer.appendChild(optionsBox);
    }else {
        cardOptionsBoxContainer.style.display = 'none';
    }
}