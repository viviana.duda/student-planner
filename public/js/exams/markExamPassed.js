function markExamPassed(examId) {
    fetch(`/exam/${examId}/status`, {
        method: 'GET',
        redirect: 'follow', 
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    }).then(_ => {
        window.location.href = '/exams';
    })
}