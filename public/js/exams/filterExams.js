function filterExams() {
    const filterButtonsContainer = document.getElementById('filter-buttons-container');

    if (!filterButtonsContainer.children.length) {
        const filterOptionsBox = document.createElement('div');
        filterOptionsBox.style.backgroundColor = 'none';
        filterOptionsBox.style.boxShadow = '0 2px 4px rgba(0, 0, 0, 0.2)';
    
        const promovateButton = document.createElement('a');
        promovateButton.innerText = 'Promovate';
        promovateButton.className = 'btn btn-primary mb-1';
        promovateButton.id = 'promoted-button';
        promovateButton.href = '/exams/passed';

        const nepromovateButton = document.createElement('a');
        nepromovateButton.innerText = 'Nepromovate';
        nepromovateButton.className = 'btn btn-primary mb-1';
        nepromovateButton.id = 'unpromoted-button';
        nepromovateButton.href = '/exams/unpassed';

        filterButtonsContainer.appendChild(promovateButton);
        filterButtonsContainer.appendChild(nepromovateButton);
    } else {
        filterButtonsContainer.innerHTML = '';
    }
}