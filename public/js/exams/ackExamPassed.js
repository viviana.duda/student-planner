function ackExamPassed(examId){
    const ackExamPassedButton = document.getElementById('exam-passed-modal-button');
    ackExamPassedButton.onclick = function() { markExamPassed(examId); };
}