function removeExam(examId) {
    fetch(`/exam/${examId}/remove`, {
        method: 'GET',
        redirect: 'follow', 
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    }).then(_ => {
        window.location.href = '/exams';
    })
}