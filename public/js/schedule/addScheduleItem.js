function addScheduleItem(scheduleItemContainerId, scheduleItemsInfoStack) {
    const scheduleItemContainer = document.getElementById(scheduleItemContainerId);
    const currentItemIndex = `${++scheduleItemsInfoStack.containerAmount[scheduleItemContainerId]}`;
    const newScheduleItemId = `${scheduleItemContainerId}-schedule-item-${currentItemIndex}`;

    let scheduleItem = document.createElement('div');
    scheduleItem.className = "schedule-item";
    scheduleItem.id = newScheduleItemId;

    scheduleItem.innerHTML = `
        <img class="remove-schedule-item" src="/images/xmark-icon.svg">
        <div class="title">
            <input type="text" name="${scheduleItemContainerId}-schedule-item-${currentItemIndex}-title">
        </div>

        <div class="time-interval">
            <img class="schedule-item-clock-icon" src="/images/dark-clock-icon.svg">

            <input type="time" name="${scheduleItemContainerId}-schedule-item-${currentItemIndex}-start-hour" value="00:00">
            <small>&nbsp;-&nbsp;</small>
            <input type="time" name="${scheduleItemContainerId}-schedule-item-${currentItemIndex}-end-hour" value="00:00">
        </div>
    `;

    const removeButton = scheduleItem.querySelector('.remove-schedule-item');
    removeButton.addEventListener('click', () => {
        removeScheduleItem(newScheduleItemId, scheduleItemsInfoStack);
    });

    const scheduleContainerLastChild = scheduleItemContainer.lastElementChild;
    scheduleItemContainer.insertBefore(scheduleItem, scheduleContainerLastChild);

    scheduleItemsInfoStack.clientSideAdded.push(newScheduleItemId);
};
