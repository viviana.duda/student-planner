function removeScheduleItem(scheduleItemId, scheduleItemsInfoStack) {
    let scheduleItemContainerId = scheduleItemId.split('-')[0];     
    let scheduleItemIsClientSideAdded = scheduleItemsInfoStack.clientSideAdded.indexOf(scheduleItemId) != -1 ? true : false;

    --scheduleItemsInfoStack.containerAmount[scheduleItemContainerId];

    if(!scheduleItemIsClientSideAdded) {
        scheduleItemsInfoStack.serverSideRemoved.push(scheduleItemId);
        scheduleItemsInfoStack.serverSideModified = scheduleItemsInfoStack.serverSideModified.filter((value) => value != scheduleItemId);
    }
    else
        scheduleItemsInfoStack.clientSideAdded = scheduleItemsInfoStack.clientSideAdded.filter((value) => value != scheduleItemId);

    document.getElementById(scheduleItemId).remove();
};