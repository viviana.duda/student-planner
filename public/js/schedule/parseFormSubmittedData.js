function parseFormSubmittedData(event, scheduleItemsInfoStack) {
    event.preventDefault(); 

    const form = event.target; 
    let formData = new FormData(form); 
    const parsedData = {
        clientSideAdded: {},
        serverSideModified: {},
        serverSideRemoved: scheduleItemsInfoStack.serverSideRemoved.map((value) => parseInt(value.split('-')[3]))
    };

    for (const pair of formData.entries()) {
        const key = pair[0];
        const value = pair[1];
        const splittedValue = key.split('-');
        const itemFullId = splittedValue.slice(0, 4).join('-');
        
        if(splittedValue.includes('schedule')) {
            const day = splittedValue[0];
            const itemId = splittedValue[3];
            const fieldType = splittedValue.length <= 5 ? 'title' : `${splittedValue[4]}_hour`;
            let itemType = '';

            if(scheduleItemsInfoStack.serverSideModified.includes(itemFullId))
                itemType = 'serverSideModified';
            else if(scheduleItemsInfoStack.clientSideAdded.includes(itemFullId))
                itemType = 'clientSideAdded';
            else    
                continue;

            if(!parsedData[itemType][day]) 
                parsedData[itemType][day] = {};
            
            if(!parsedData[itemType][day][itemId]) 
                parsedData[itemType][day][itemId] = {};

            parsedData[itemType][day][itemId][fieldType] = value;
        }
    }

    if(Object.keys(parsedData.clientSideAdded) || Object.keys(parsedData.serverSideModified) || Object.keys(parsedData.serverSideRemoved))
        fetch('/schedule', {
            method: 'PUT',
            redirect: 'follow', 
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },
            body: JSON.stringify({
                parsedData
            })
        }).then(response => {
            window.location.href = '/schedule';
        })
};