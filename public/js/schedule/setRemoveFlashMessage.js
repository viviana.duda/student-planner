function setRemoveFlashMessage() {
    setTimeout(() => {
        const flashMessage = document.getElementById('flash-message-schedule-updated');
        flashMessage.remove();
    }, 1500);
}