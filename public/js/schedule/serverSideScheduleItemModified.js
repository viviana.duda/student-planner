function serverSideScheduleItemModified(scheduleItemId, scheduleItemsInfoStack) {
    if(!scheduleItemsInfoStack.serverSideModified.includes(scheduleItemId))
        scheduleItemsInfoStack.serverSideModified.push(scheduleItemId);
};