
function updateCalendarSchedule() {
    const scheduleItems = calendarData['currentDayScheduleItems'];

    if(!scheduleItems.length)
        return;
    
    const currentDateTime = dayjs(); 

    function getNextTimeUpdateTimestamp() {
        const lastScheduleItemEndHour = scheduleItems[0]['end_hour'];
        const lastScheduleItemEndHourParsedHour = parseInt(lastScheduleItemEndHour.split(':')[0]);
        const lastScheduleItemEndHourParsedMinutes = parseInt(lastScheduleItemEndHour.split(':')[1]);

        let nextTimeUpdate = currentDateTime.set('hour', lastScheduleItemEndHourParsedHour).set('minute', lastScheduleItemEndHourParsedMinutes).valueOf() - dayjs().valueOf();
        
        if(nextTimeUpdate <= 0)
            nextTimeUpdate += 6000;

        return nextTimeUpdate;
    }

    const updateCalendarAt = getNextTimeUpdateTimestamp();

    setTimeout(() => {
        let scheduleItemsContainer = document.getElementById('schedule-items');
        scheduleItemsContainer.innerHTML = '';

        fetch('/schedule/items', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            }
        }).then((response) => {
            response.json().then((data) => {
                let responseScheduleItems = data.scheduleItems;
                if(responseScheduleItems.length) {
                    for(i = 0; i < responseScheduleItems.length; ++i){
                        const scheduleItem = document.createElement('div');
                        scheduleItem.id = `schedule-item-${i}`;
                        scheduleItem.innerHTML = `                            
                            <h5 id="schedule-item-${i}-interval">${responseScheduleItems[i]['start_hour']} - ${responseScheduleItems[i]['end_hour']}</h5>
                            <h5 id="schedule-item-${i}-title">${responseScheduleItems[i]['title']}</h5>
                        `;

                        scheduleItemsContainer.appendChild(scheduleItem);
                    }


                    calendarData['currentDayScheduleItems'] = responseScheduleItems;
                    setTimeout(updateCalendarSchedule, getNextTimeUpdateTimestamp());
                }else {
                    let noCourseTodayMessage = document.createElement('h1');
                    noCourseTodayMessage.innerHTML = `Nici-un curs astazi`;
                    noCourseTodayMessage.id = 'no-course-today-message';

                    scheduleItemsContainer.appendChild(noCourseTodayMessage);
                }
        })
        }).catch((error) => {   
            setTimeout(updateCalendarSchedule, 60000);
        })

    }, updateCalendarAt);
}
