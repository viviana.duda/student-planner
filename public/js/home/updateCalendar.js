function updateCalendar() {
    
    // Right Panel
    const calendarPreviousMonth = document.getElementById('previous').querySelector('.month');
    const calendarPreviousMonthYear = document.getElementById('previous').querySelector('.year')

    const calendarCurrentMonth = document.getElementById('current').querySelector('.month');
    const calendarCurrentYear = document.getElementById('current').querySelector('.year')

    const calendarFollowingMonth = document.getElementById('following').querySelector('.month');
    const calendarFollowingMonthYear = document.getElementById('following').querySelector('.year')

    const previousMonth = currentDate.month() - 1;
    const currentMonth = currentDate.month();
    const followingMonth = currentDate.month() + 1;
    const currentYear = currentDate.year();
    const currentDay = currentDate.date();

    calendarPreviousMonth.innerHTML = monthMap[previousMonth];
    calendarPreviousMonthYear.innerHTML = (currentMonth - 1) ==  -1 ? (currentYear - 1) : currentYear;
    calendarPreviousMonth.style.opacity = '0.5';
    calendarPreviousMonthYear.style.opacity = '0.5';

    calendarCurrentMonth.innerHTML = monthMap[currentDate.month()];
    calendarCurrentYear.innerHTML = currentDate.year();

    calendarFollowingMonth.innerHTML = monthMap[followingMonth];
    calendarFollowingMonthYear.innerHTML = (currentMonth + 1) ==  12 ? (currentYear + 1) : currentYear;
    calendarFollowingMonth.style.opacity = '0.5';
    calendarFollowingMonthYear.style.opacity = '0.5';
    //


    // Left Panel
    const leftPanelCurrentMonth = document.getElementById('current-date-month');
    const leftPanelCurrentYear = document.getElementById('current-date-year');
    const leftPanelCurrentDay = document.getElementById('current-date-day');
    leftPanelCurrentDay.innerHTML = currentDay < 10 ? `0${currentDay}` : currentDay;
    leftPanelCurrentMonth.innerHTML = monthMap[currentMonth];
    leftPanelCurrentYear.innerHTML = currentYear;
    //
}
