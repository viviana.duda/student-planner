function setCalendarItems() {
    const month = dayjs().month() + 1; 
    const year = dayjs().year();
    const daysInMonth = dayjs(`${year}-${month}`).daysInMonth();
    const calendar = [];

    function dayRelativeExam(day) {
        let dayRelativeToExamDay = false;

        for(let exam of calendarData['exams']) {
            let examDate = parseInt(exam['date'].split('-')[2]);

            if(examDate == day) {

                dayRelativeToExamDay = true;
                break;
            }
        }

        return dayRelativeToExamDay;
    }

    function dayRelativeTask(day) {
        let dayRelativeToTaskDay = false;

        for(let task of calendarData['tasks']) {
            let taskDeadline = parseInt(task['deadline'].split('-')[2]);

            if(taskDeadline == day) {
                dayRelativeToTaskDay = true;
                break;
            }
        }

        return dayRelativeToTaskDay;
    }

    for (let day = 1; day <= daysInMonth; day++) {
        const date = dayjs(`${year}-${month}-${day}`);
        const numericValue = date.date();
        const stringValue = date.format('dddd').toLowerCase(); 

        calendar.push({
            numericValue,
            stringValue
        });
    };

    for(let day of calendar){
        let dayContainer = document.getElementById(day['stringValue']);
        let dayItem = document.createElement('div');

        if(dayRelativeExam(day['numericValue']))
            dayItem.style.backgroundColor = '#F99393';
        else if(dayRelativeTask(day['numericValue'])) 
            dayItem.style.backgroundColor = '#FF7527';

        dayItem.id = `${day['stringValue']}-${day['numericValue']}`;
        dayItem.innerHTML = `
            <h1>${day['numericValue']}</h1>
        `;

        dayContainer.appendChild(dayItem);
    }
}  