function clientInformationsLogin(event) {
    event.preventDefault(); 
    let form = document.getElementById('login-form');

    let clientTimeZone = document.createElement('input');
    clientTimeZone.type = 'hidden';
    clientTimeZone.name = 'client_time_zone';
    clientTimeZone.value = dayjs.tz.guess();

    form.appendChild(clientTimeZone);

    form.submit();
}