<?php

namespace App\Services;

use Carbon\Carbon;



class Helpers {
    
    public static function fieldRename($array, $key, $newKeyName) {
        for($i = 0; $i < count($array); ++$i){
            if(isset($array[$i][$key])) {
                $array[$i][$newKeyName] = $array[$i][$key];
                unset($array[$i][$key]);
            }
        }

        return $array;
    }

    public static function mapKeys($map, $object) {
        $mappedData = [];
        foreach(array_keys($map) as $key) {
            $mappedData[$key] = null;

            if(is_array($map[$key])){
                foreach($map[$key] as $subKey){
                    if(isset($object[$subKey])){
                        $mappedData[$key] = $object[$subKey];
                        break;
                    }
                }
            }
            else if(is_callable($map[$key])) {
                $mappedData[$key] = $map[$key]($object);
            }
        }

        return $mappedData;
    }

    public static function processEvents($exams, $tasks = null){
        $keysMap = [
            "id" => ['id'],
            "title" => ['course_name', 'title'],
            "subtitle" => function($event) {
                if(isset($event['title']))
                    return 'Task';
                else
                    return 'Exam';
            },
            "location" => ['class_name'],
            "period" => ['date'],
            "duration" => ['duration'],
            "calendar_color" => ['calendar_color'],
            "alert_ack" => ['alert_ack']
        ];

        $prioritizeEvents = function($exams, $tasks) {
            $events = array_merge($exams, $tasks);

            for($i = 0; $i < count($events); ++$i) {
                for($j = $i + 1; $j < count($events); ++$j) {
                    $iDate = Carbon::parse($events[$i]['date']);
                    $jDate = Carbon::parse($events[$j]['date']);

                    if($jDate->lt($iDate)) {
                        $temp = $events[$i];
                        $events[$i] = $events[$j];
                        $events[$j] = $temp;
                    }
                }
            }

            return $events;
        };

        $tasks = Helpers::fieldRename($tasks, 'deadline', 'date');

        $prioritizedEvents = $prioritizeEvents($exams, $tasks);
        
        $processedEvents = []; 

        foreach($prioritizedEvents as $prioritizedEvent) 
            array_push($processedEvents, Helpers::mapKeys($keysMap, $prioritizedEvent));

        return $processedEvents;
    }

    public static function formatDateToTime(&$events) {
        foreach($events as &$event) {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $event['period']);
            if(isset($event['duration'])) {
                
                $event['period'] = [
                    'start_time' => $date->format('h:i A'), 
                    'end_time' =>  $date->add($event['duration'], 'minute')->format('h:i A')
                ];

            }
            else {

                $event['period'] = [
                    'start_time' => null,
                    'end_time' => $date->format('h:i A')
                ]; 
            }
                
        }
    }

    public static function formatForCalendar($events) {
        $formattedEvents = [

        ];

        foreach($events as $event) {
            if($event['subtitle'] == 'Exam') {

                array_push($formattedEvents, [
                    "title" => $event['subtitle'],
                    "start" => $event['period']
                ]);

            }else if($event['subtitle'] == 'Task') {
                array_push($formattedEvents, [
                    "title" => $event['subtitle'],
                    "start" => $event['period']
                ]);
            }

          
        }


        return $formattedEvents;
    }
}