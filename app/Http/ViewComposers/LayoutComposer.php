<?php

namespace App\Http\ViewComposers;

use Throwable;
use Carbon\Carbon;
use App\Models\Exam;
use App\Models\Task;
use App\Services\Helpers;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;

class LayoutComposer
{
    public function compose(View $view)
    {
        $clientTimeZone = auth()->user()->client_time_zone;

        $layoutData = [
            "todayEvents" => [],
            "tommorowEvents" => [],
            "userProfileImagePath" => null
        ];

        try {
            $userProfileImageAbsolutePath = 'public/userVault/' . auth()->id() . '/profile-image.png'; 

            if (Storage::disk('local')->exists($userProfileImageAbsolutePath))
                $layoutData['userProfileImagePath'] = $userProfileImageAbsolutePath;

        }catch(Throwable $error) {
            // ignore
        } 

        try { 
            $tommorrowStartDate = Carbon::tomorrow()->tz($clientTimeZone)->format('Y-m-d H:i:s');
            $tommorrowEndDate = Carbon::tomorrow()->endOfDay()->tz($clientTimeZone)->format('Y-m-d H:i:s');

            $todayTasks = Task::todayDeadlineTasks()->get()->toArray();
            $todayExams = Exam::todayExams()->get()->toArray();

            $tommorowTasks = Task::getByInterval($tommorrowStartDate, $tommorrowEndDate)->get()->toArray();
            $tommorowExams = Exam::getByInterval($tommorrowStartDate, $tommorrowEndDate)->get()->toArray();

            $todayProcessedEvents = Helpers::processEvents($todayExams, $todayTasks);
            $layoutData['todayEvents'] = $todayProcessedEvents;

            $tommorowProcessedEvents = Helpers::processEvents($tommorowExams, $tommorowTasks);
            $layoutData['tommorowEvents'] = $tommorowProcessedEvents;

            Helpers::formatDateToTime($layoutData['todayEvents']);
            Helpers::formatDateToTime($layoutData['tommorowEvents']);

        }catch(Throwable $error) {
            // ignore
        }


        $view->with('layoutData', $layoutData);
    }
}
