<?php

namespace App\Http\Controllers;

use Throwable;
use Carbon\Carbon;
use App\Models\User;
use App\Services\Helpers;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public static function show() {
        try {
            $authUser = auth()->user();
            $userCurrentDayTime = Carbon::now()->tz(auth()->user()->clientTimeZone)->format('Y-m-d H:i:s');
            $formattedEvents = [];
            
            $tasks = User::find($authUser->id)->latestTasks($userCurrentDayTime);
            $exams = User::find($authUser->id)->latestExams($userCurrentDayTime);

            $processedEvents = Helpers::processEvents($exams, $tasks);
            $formattedEvents = Helpers::formatForCalendar($processedEvents);

            return view('calendar.show', ['calendarData' => $formattedEvents]);
        }catch(Throwable $error) {
            dd($error);
            $reason = "Pagina de calendar nu poate fi procesata de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }
    }
}
