<?php

namespace App\Http\Controllers;

use App\Models\Exam;
use App\Models\Task;
use App\Models\User;
use App\Models\ScheduleItem;
use Illuminate\Http\Request;
use Throwable;

class NotificationsController extends Controller
{

    public static function getNotifications() {
        $notifications = null;

        try {

            $authUser = User::find(auth()->id());
    
            if(isset($authUser))
                $notifications = $authUser->notifications();

        }catch(Throwable $error) {
            // ignore
        }
        
        return response()->json($notifications);
    }
    

     public static function acknowledge(Request $request) {
        $alerts = $request->all();

        try {

            foreach($alerts as $alert) {
                if($alert['eventType'] == 'Exam') {
                    $exam = Exam::where('id', $alert['id'])->first();
                    if(!isset($exam))
                        continue; 
    
                    $exam->alert_ack = true;
                    $exam->save();
                }else if($alert['eventType'] == 'Task') {
                    $task = Task::where('id', $alert['id'])->first();
                    if(!isset($task))
                        continue; 
                
                    $task->alert_ack = true;
                    $task->save();
                }else {
                    $scheduleItem = ScheduleItem::where('id', $alert['id'])->first();
                    if(!isset($scheduleItem))
                        continue;   
    
                    $scheduleItem->alert_ack = true;
                    $scheduleItem->save();
                }
            }

        }catch(Throwable $error) {
            // ignore            
        }
     
    }

}
