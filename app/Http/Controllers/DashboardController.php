<?php

namespace App\Http\Controllers;

use App\Models\User;
use Throwable;

class DashboardController extends Controller
{

    public function show() {
        
        $userStatistics = null;

        try {
            
            $authUser = User::find(auth()->id());
            $userStatistics = $authUser->statistics();

        }catch(Throwable $error) {

            $reason = "Pagina cu statistici nu a putut fi trimisa de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }
      

        return view('dashboard.show', ['statistics' => $userStatistics]);
    }
}
