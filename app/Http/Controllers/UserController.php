<?php

namespace App\Http\Controllers;

use Throwable;
use Carbon\Carbon;
use App\Models\Exam;
use App\Models\Task;
use App\Models\User;
use App\Models\Faculty;
use App\Models\Schedule;
use App\Services\Helpers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public static function showProfile() {
        $availableFaculties = Faculty::all();

        return view('user.show', ["faculties" => $availableFaculties]);
    }

    public static function store(Request $request) { 
        $userDetails = $request->validate([
            "name" => 'required',
            "email" => ['required', 'email', Rule::unique('users', 'email')],
            "password" => 'required',
            "faculty_id" => ['required', Rule::exists('faculties', 'id')],
            "client_time_zone" => "sometimes"
        ],
        [
            "name.required" => "Numele trebuie introdus!",
            "email.required" => "E-mail-ul trebuie introdus!",
            "password.required" => "Parola trebuie introdusa!"
        ]);       
        
        $user = null;

        try {
            DB::transaction(function() use ($userDetails, &$user) {
                $userDetails['faculty_id'] = intval($userDetails['faculty_id']);
                $user = new User($userDetails);
                $user->password=bcrypt($user->password);
                $user->save();
            
                Schedule::create([
                    'user_id' => $user->id
                ]);
            });
        }catch(Throwable $error) {
            $reason = "Procesul de inregistrare nu a putut fi procesat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 503
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        if(isset($user))
            auth()->login($user);

        return redirect('/');
    }

    public static function register() {
        $availableFaculties = Faculty::all();

        return view('user.register', ["faculties" => $availableFaculties]);
    }

    public static function authenticate(Request $request) {
        $userDetails = $request->validate([
            "email" => ["required", "email"],
            "password" => "required",
            "clientTimeZone" => "sometimes"
        ],
        [
            "email.required" => "E-mail-ul trebuie introdus!",
            "password.required" => "Parola trebuie introdusa!",
        ]);
        
        $clientTimeZone = null;

        if(isset($userDetails['clientTimeZone'])) {
            $clientTimeZone = $userDetails['clientTimeZone'];
            unset($userDetails['clientTimeZone']);
        }   
        
        try {
            if(auth()->attempt($userDetails)){
                $request->session()->regenerate();
                if(isset($clientTimeZone)) {
                    $user = User::find(auth()->id());
    
                    $user->update(['client_time_zone' => $clientTimeZone]);
                }
    
                return redirect('/');
            }
        }catch(Throwable $error) {
            $reason = "Procesul de autentificare nu a putut fi procesat de catre server!";

            return view('errors.500', ["reason" => $reason]);
        }

        return back()->withErrors(['email' => "Datele sunt incorecte"]);
    }

    public static function login() {
        return view('user.login');
    }

    public static function logout() {

        auth()->logout();
        session()->invalidate();
        session()->regenerateToken();

        return redirect('/login');
    }

    public static function update(Request $request) {
        $authUser = User::find(auth()->id());

        $userUpdatedDetails = $request->validate([
            "name" => "sometimes",
            "password" => "sometimes",
            "faculty_id" => "required",
            "profile_image" => "sometimes"
        ]);

        try {
            $userUpdatedDetails = array_filter($userUpdatedDetails);
            $userUpdatedDetails['faculty_id'] = (int) $userUpdatedDetails['faculty_id'];

            if(isset($userUpdatedDetails['password'])) 
                $userUpdatedDetails['password'] = bcrypt($userUpdatedDetails['password']);

            if(isset($userUpdatedDetails['profile_image'])) {
                $profileImageVaultAbsolutePath = 'public/userVault/' . auth()->id() .'/' . 'profile-image.png'; 
                if (Storage::disk('local')->exists($profileImageVaultAbsolutePath))  
                    Storage::delete($profileImageVaultAbsolutePath);    
                
                Storage::disk('local')->put($profileImageVaultAbsolutePath,  file_get_contents($request->file('profile_image')));
            }

            unset($userUpdatedDetails['profile_image']);
            $authUser->update($userUpdatedDetails);
        }catch(Throwable $error) {
            $reason = "Procesul de modificare a datelor nu a putut fi executat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        return redirect('/');
    }

    public static function getEvents(Request $request) {
        $rules = [
            "eventsPeriod" => 'required|in:interval,today,tommorow',
            "dateInterval" => 'required_if:eventsPeriod,interval',
            "dateInterval.start_date" => 'required_if:eventsPeriod,interval|date_format:Y-m-d H:i:s',
            "dateInterval.end_date" => 'required_if:eventsPeriod,interval|date_format:Y-m-d H:i:s'
        ];

        $data = null; 

        try {
            $data = $request->validate($rules);
        }catch(Throwable $error) {
            return response($error, 400);
        }

        $eventPeriodType = $data['eventsPeriod'];
        
        $tasks = null;
        $exams = null;

        try {
            $clientTimeZone = auth()->user()->client_time_zone;

            if($eventPeriodType == 'interval'){

                $dateInterval = $request->input('dateInterval');

                $start_date = $dateInterval['start_date'];
                $end_date = $dateInterval['end_date'];

                $start_date = Carbon::parse($start_date)->tz($clientTimeZone)->format('Y-m-d H:i:s');
                $end_date = Carbon::parse($end_date)->tz($clientTimeZone)->format('Y-m-d H:i:s');

                $tasks = Task::getByInterval($start_date, $end_date)->get()->toArray();
                $exams = Exam::getByInterval($start_date, $end_date)->get()->toArray();

            }
            else if($eventPeriodType == 'today')  {

                $exams = Exam::todayExams()->get()->toArray();
                $tasks = Task::todayDeadlineTasks()->get()->toArray();

            }
            else if($eventPeriodType == 'tommorow') {

                $tommorrowStartDate = Carbon::tomorrow()->tz($clientTimeZone)->format('Y-m-d H:i:s');
                $tommorrowEndDate = Carbon::tomorrow()->tz($clientTimeZone)->format('Y-m-d H:i:s');

                $tasks = Task::getByInterval($tommorrowStartDate, $tommorrowEndDate)->get()->toArray();
                $exams = Exam::getByInterval($tommorrowStartDate, $tommorrowEndDate)->get()->toArray();
            }
            
            $processedEvents = Helpers::processEvents($exams, $tasks);
            Helpers::formatDateToTime($processedEvents);

            return response()->json($processedEvents);
            
        }catch(Throwable $error) {
            return response($error, 500);
        }
    }

}
