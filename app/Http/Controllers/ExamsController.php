<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Exam;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;

class ExamsController extends Controller
{

    public static function index(Request $request, $filter = 'default') {
        try {
            $authUser = auth()->user();
            $exams = null;

            if($filter == 'passed')
                $exams = Exam::where('user_id', $authUser->id)
                                ->where('passed', 1)
                                ->paginate(3);

            else if($filter == 'unpassed')
                $exams = Exam::where('user_id', $authUser->id)
                                ->where('passed', 0)
                                ->paginate(3);
            else
                $exams = Exam::where('user_id', $authUser->id)
                                ->paginate(3);
                            
            foreach($exams as $exam) {
                $date = Carbon::createFromFormat('Y-m-d H:i:s', $exam['date']);
                $exam['start_hour'] = $date->format('h:i A');
                $exam['date'] = $date->format('Y-m-d');
            }
    
            return view('exams.index', compact('exams'));
        }catch(Throwable $error) {
            $reason = "Procesul de vizualizare a examenelor nu a putut fi executat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }
    }

    public static function update(Request $request, $examId) {
        $requestPayload = $request->validate([
            "course_name" => "required",
            "date" => "required",
            "duration" => "required",
            "class_name" => "required",
            "teacher_name" => "required",
            "calendar_color" => "sometimes"
        ],
        [
            "course_name.required" => "Materia trebuie sa fie completata!",
            "date.required" => "Data trebuie sa fie setata!",
            "teacher_name.required" => "Profesorul trebuie sa fie adaugat!",
            "duration.required" => "Durata trebuie sa fie setata!",
            "class_name.required" => "Clasa trebuie sa fie setata!",
        ]);

        try {
         
            $exam = Exam::where('user_id', auth()->id())
                        ->where('id',  $examId)
                        ->get()
                        ->first();
            
            if(isset($exam))
                $exam->update($requestPayload);

        }catch(Throwable $error) {
            $reason = "Procesul de modificare a examenului nu a putut fi executat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        return redirect('/exams');
    }

    public static function edit($examId) {
        try {

            $authUser = auth()->user();
            $exam = Exam::where('user_id', $authUser->id)
                    ->where('id',  $examId)
                    ->get()
                    ->first();

            if(isset($exam))
                return view('exams.edit', ["exam" => $exam]);

        }catch(Throwable $error) {
            $reason = "Procesul de editare a examenului nu poate fi executat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        
       return redirect('/');
    }

    public static function create() {
        return view('exams.create');
    }

    public static function store(Request $request) {

        $requestPayload = $request->validate([
            "course_name" => "required",
            "date" => "required",
            "duration" => "required",
            "class_name" => "required",
            "teacher_name" => "required",
            "calendar_color" => "sometimes"
        ],
        [
            "course_name.required" => "Materia trebuie sa fie completata!",
            "date.required" => "Data trebuie sa fie setata!",
            "teacher_name.required" => "Profesorul trebuie sa fie adaugat!",
            "duration.required" => "Durata trebuie sa fie setata!",
            "class_name.required" => "Clasa trebuie sa fie setata!",
        ]
        );

        try {

            $userId = auth()->id();
            $requestPayload['user_id'] = $userId;
            Exam::create($requestPayload);

        }catch(Throwable $error) {
            $reason = "Procesul de creare a examenului nu a putut fi executat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        return redirect('/exams');
    }

    public static function updateStatus($examId) {

        try {

            $exam = Exam::where('user_id', auth()->id())
                    ->where('id',  $examId)
                    ->get()
                    ->first();
                

            if(isset($exam)) {
                $exam->passed = 1;
                $exam->save();
            }

        }catch(Throwable $error) {
            // ignore
        }
     
        return redirect('/exams');
    }

    public static function remove($examId) {
        try {

            $exam = Exam::where('user_id', auth()->id())
                    ->where('id',  $examId)
                    ->get()
                    ->first();

            if(isset($exam) && $exam->passed == 1)
                $exam->delete();

        }catch(Throwable $error) {
            // ignore
        }

        return redirect('/exams');
    }
}
