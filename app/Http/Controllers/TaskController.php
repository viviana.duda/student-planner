<?php

namespace App\Http\Controllers;

use Throwable;
use App\Models\Task;
use App\Models\TaskFile;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TaskController extends Controller
{

    public static function showOverview($taskId) {
        try {
            $authUser = auth()->user();
            $task = Task::where('user_id', $authUser->id)
                    ->where('id', $taskId)
                    ->get()
                    ->toArray();
            
            if(isset($task[0]))
                return view('tasks.overview', ["task" => $task[0]]);

        }catch(Throwable $error) {

            $reason = "Procesul de vizualizare a task-ului nu a putut fi procesat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        return redirect('/');
    }


    public static function store(Request $request) {
        $requestPayload = request()->validate([
            "title" => "required",
            "course" => "required",
            "deadline" => "required",
            "description" => "required",
            "files" => "sometimes"
        ],
        [
            "title.required" => "Titlul trebuie sa fie completat!",
            "course.required" => "Materia trebuie sa fie completata!",
            "deadline.required" => "Deadline-ul trebuie sa fie setat!",
            "description.required" => "Descrierea trebuie sa fie completata!",
        ]);

        try {
            
            DB::transaction(function() use ($requestPayload, $request) {
                $userId = Auth::user()->id;
                $requestPayload['user_id'] = $userId;
                $task = Task::create($requestPayload);

                if(isset($requestPayload['files'])) {
                    foreach($request->file('files') as $file) {
                        
                        $fileName = $file->getClientOriginalName();
                        $fileId = time(). Str::random(7);

                        $taskVaultAbsolutePath = '/userVault/' . $userId . '/tasks/' . $task->id . '/' . $fileId . '/' . $fileName; 

                        Storage::disk('local')->put($taskVaultAbsolutePath,  file_get_contents($file));
                        
                        TaskFile::create([
                            "task_id" => $task->id,
                            "file_storage_id" => $fileId,
                            "file_name" => $fileName
                        ]);
                    }
                }
            });

        }catch(Throwable $error) {
            $reason = "Procesul de stocare a task-ului nu a putut fi procesat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }
   
        return redirect('/tasks');
    }

    public static function index() {
        try {
            $authUser = auth()->user();
            $tasks = Task::where('user_id', $authUser->id)->paginate(3);

            
            return view('tasks.index', compact('tasks'));
        }catch(Throwable $error) {
            $reason = "Procesul de vizualizare a task-urilor nu a putut fi executat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }
    }

    public static function create() {
        return view('tasks.create');
    }

    public static function updateProgress()
    {
        try {

            $requestPayload = request()->validate([
                "id" => ["required",
                    Rule::exists('tasks')->where('user_id', auth()->id())
                ],
                "progress_percentage" => "required"
            ]);
    
            $taskEntity = Task::find($requestPayload['id']);
            $taskEntity->progress_percentage = intval($requestPayload['progress_percentage']);
            $taskEntity->save();
            
        }catch(Throwable $error) { 
            // ignore
        }
    }

    public static function edit($taskId) {
        try {
            
            $authUser = auth()->user();
            $task = Task::where('user_id', $authUser->id)
                    ->where('id', $taskId)
                    ->get()
                    ->toArray();
            
            if(isset($task[0]))
                return view('tasks.edit', ["task" => $task[0]]);

        }catch(Throwable $error) {

            $reason = "Procesul de editare a task-ului nu a putut fi procesat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        return redirect('/');
    }

    public static function update(Request $request, $taskId) {
        $requestPayload = $request->validate([
                "title" => "required",
                "deadline" => "required",
                "course" => "required",
                "description" => "required",
                "files" => "sometimes"
            ],
            [
                "title.required" => "Titlul trebuie sa fie completat!",
                "course.required" => "Materia trebuie sa fie completata!",
                "deadline.required" => "Deadline-ul trebuie sa fie setat!",
                "description.required" => "Descrierea trebuie sa fie completata!",
            ]);

        try {


            DB::transaction(function() use ($taskId, $requestPayload, $request) {

                $task = Task::where('user_id', auth()->id())
                    ->where('id',  $taskId);
            
                $requestFiles = [];
                
                if(isset($requestPayload['files'])) {
                    $requestFiles = $requestPayload['files'];
                    unset($requestPayload['files']);
                }

                if(isset($task))
                    $task->update($requestPayload);

                if(isset($requestFiles)) {
                    foreach($requestFiles as $file) {
                        
                        $fileName = $file->getClientOriginalName();
                        $fileId = time(). Str::random(7);

                        $taskVaultAbsolutePath = '/userVault/' . auth()->id() . '/tasks/' . $taskId . '/' . $fileId . '/' . $fileName; 

                        Storage::disk('local')->put($taskVaultAbsolutePath,  file_get_contents($file));
                        
                        TaskFile::create([
                            "task_id" => $taskId,
                            "file_storage_id" => $fileId,
                            "file_name" => $fileName
                        ]);
                    }
                }
            });

        }catch(Throwable $error) {
            dd($error);
            $reason = "Procesul de modificare a task-ului nu a putut fi procesat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }
        
        return redirect('/tasks');
    }

    public static function remove($taskId) {

        try {
            $task = Task::where('user_id', auth()->id())
                    ->where('id',  $taskId)
                    ->get();
    
            if(isset($task[0]))
                $task[0]->delete();
        }catch(Throwable $error) {
            $reason = "Procesul de stergere a task-ului nu a putut fi procesat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }

        return redirect('/tasks');
    }

    public static function getFiles($taskId) {
        $authUserId = auth()->id();
        $filesData = [];

        try {

            $task = Task::where('id', $taskId)
                        ->where('user_id', $authUserId)
                        ->get()
                        ->first();

            if(isset($task)) {
                $taskFiles = $task->files;

                foreach ($taskFiles as $taskFile) {
                    array_push($filesData, [
                        "fileName" => $taskFile->file_name,
                        "fileId" => $taskFile->file_storage_id
                    ]);
                }
            }

        }catch(Throwable $error) {
            $reason = "Procesul de vizualizare de fisiere nu a putut fi procesat de catre server";
            $errorDetails = [
                "reason" => $reason,
                "code" => 500
            ];

            return view('error', [ "errorData" => $errorDetails ]);
        }


        return response()->json($filesData);
    }

    public static function downloadFile($taskId, $fileId) {
        try {
            $task = Task::where('id', $taskId)
                        ->where('user_id', auth()->id())
                        ->get()
                        ->first();

            if(isset($task)) {
                $taskFile = TaskFile::where('task_id', $task->id)
                                    ->where('file_storage_id', $fileId)
                                    ->get()
                                    ->first();

                if(isset($taskFile)) {
                    $taskFileAbsolutePath = '/userVault/' . $task->user_id . '/tasks/' . $task->id . '/' . $taskFile->file_storage_id . '/' . $taskFile->file_name; 

                    if (Storage::disk('local')->exists($taskFileAbsolutePath)) 
                        return response()->download(storage_path('app/') . $taskFileAbsolutePath, $taskFile->file_name);
                }
            }
        }catch(Throwable $error) {
            // ignore
        }
    }

    public static function removeFile($taskId, $fileId) {
        try {
            $task = Task::where('id', $taskId)
                        ->where('user_id', auth()->id())
                        ->get()
                        ->first();

            if(isset($task)) {
                $taskFile = TaskFile::where('task_id', $task->id)
                                      ->where('file_storage_id', $fileId)
                                      ->get()
                                      ->first();

                if(isset($taskFile)) {
                    DB::transaction(function() use ($taskFile, $task) {
                        $taskFile->delete();
                        $taskFileAbsolutePath = '/userVault/' . $task->user_id . '/tasks/' . $task->id . '/' . $taskFile->file_storage_id . '/' . $taskFile->file_name; 

                        Storage::delete(storage_path('app/') . $taskFileAbsolutePath);
                    });
                }
            }
        }catch(Throwable $error) {
            // ignore            
        }
    }
}
 