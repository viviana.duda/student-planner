<?php

namespace App\Http\Controllers;

use Throwable;
use App\Models\User;
use App\Models\ScheduleItem;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public static function show() {
    
        $orderedScheduleItems = [
            "monday" => [],
            "tuesday" => [],
            "wednesday" => [],
            "thursday" => [],
            "friday" => [],
            "saturday" => [],
            "sunday" => []
        ];

        try {
            $userSchedule = auth()->user()->schedule;
            $scheduleItems = $userSchedule->items;
                
            $dayMap = [
                0 => 'monday',
                1 => 'tuesday',
                2 => 'wednesday',
                3 => 'thursday',
                4 => 'friday',
                5 => 'saturday',
                6 => 'sunday'
            ];
        
            if(isset($scheduleItems)){
                foreach($scheduleItems as $key => $scheduleItemValue) {
                    array_push($orderedScheduleItems[$dayMap[$scheduleItemValue['day']]], $scheduleItemValue);
                }
            }

        }catch(Throwable $error) {
            // ignore            
        }
        
        return view('schedule.show', ["scheduleItems" => $orderedScheduleItems]);
    }

    public static function update(Request $request) {
       
        $dayMap = [
            'monday' => 0,
            'tuesday' => 1,
            'wednesday' => 2,
            'thursday' => 3,
            'friday' => 4,
            'saturday' => 5,
            'sunday' => 6,
        ]; 
        
        $data = $request->validate([
            "parsedData" => 'required',
        ])['parsedData'];
      
        try {

            $authUser = auth()->user();
            $userScheduleId = $authUser->schedule;
        
            foreach($data['clientSideAdded'] as $day => $scheduleItems) {
                foreach($scheduleItems as $scheduleItem){
                    $scheduleItem['schedule_id'] = $userScheduleId->id;
                    $scheduleItem['day'] = $dayMap[$day];
        
                    ScheduleItem::create($scheduleItem);
                }
            };
    
            foreach($data['serverSideModified'] as $day => $scheduleItems) {
                foreach($scheduleItems as $itemId => $scheduleItem){
                    $existingScheduleItem = ScheduleItem::where('schedule_id', $userScheduleId->id)
                                            ->where('id', $itemId)
                                            ->first();
                    if($existingScheduleItem)
                        $existingScheduleItem->update($scheduleItem);
                }
            };
    
            foreach($data['serverSideRemoved'] as $itemId) {
                $existingScheduleItem = ScheduleItem::where('schedule_id', $userScheduleId->id)
                                                    ->where('id', $itemId)
                                                    ->first();
                                                        
                if($existingScheduleItem)
                    $existingScheduleItem->delete();
            };

        }catch(Throwable $error) {
            // ignore
        }

        session()->flash('message', 'Orar actualizat!');
    }

    public static function latestScheduleItems() {

        $scheduleItems = [];

        try {

            $calendarData = User::find(auth()->id())->calendarData();

            if(isset($calendarData))
                $scheduleItems['scheduleItems'] = $calendarData['currentDayScheduleItems'];

        }catch(Throwable $error) {
            // ignore
        }
        
        return response()->json($scheduleItems);
    }
}
