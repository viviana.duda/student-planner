<?php

namespace App\Models;

use Throwable;
use Carbon\Carbon;

use App\Models\TaskFile;
use App\Services\Helpers;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'faculty_id',
        'client_time_zone'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tasks() {
        return $this->hasMany(Task::class, 'user_id');
    }

    public function exams() {
        return $this->hasMany(Exam::class, 'user_id');
    }

    public function schedule() {
        return $this->hasOne(Schedule::class, 'user_id');
    }

    public function statistics() {
        $statisticsData = [
            "createdTasks" => [0, 0, 0, 0, 0, 0, 0],
            "createdExams" => [0, 0, 0, 0, 0, 0, 0],
            "passedExams" =>  [0, 0, 0, 0, 0, 0, 0],
            "finishedTasks" => [0, 0, 0, 0, 0, 0, 0]
        ];

        $dayMap = [
            0 => 6,
            1 => 0,
            2 => 1,
            3 => 2,
            4 => 3,
            5 => 4,
            6 => 5
        ];

        $userExams = $this->exams;
        $userTasks = $this->tasks;
        
        $currentDayTime = Carbon::now()->tz($this->clientTimeZone);

        foreach($userExams as $userExam) {
            $startOfWeek = $currentDayTime->startOfWeek(Carbon::MONDAY);
            $examCreationDate = Carbon::parse($userExam['created_at'], $this->clientTimeZone);
            $examUpdateDate = Carbon::parse($userExam['updated_at'], $this->clientTimeZone);

            if($examCreationDate->gt($startOfWeek)){
                $examCreationDay = $dayMap[$examCreationDate->dayOfWeek];
                $examPassedDay = $dayMap[$examUpdateDate->dayOfWeek];

                if($userExam['passed'] == 1)
                    $statisticsData['passedExams'][$examPassedDay] += 1;
                        
                $statisticsData['createdExams'][$examCreationDay] += 1;
            }
        }

        foreach($userTasks as $userTask) {
            $startOfWeek = $currentDayTime->startOfWeek(Carbon::MONDAY);
            $taskCreationDate = Carbon::parse($userTask['created_at'], $this->clientTimeZone);
            $taskUpdateDate = Carbon::parse($userTask['updated_at'], $this->clientTimeZone);

            if($taskCreationDate->gt($startOfWeek)){
                $taskCreationDate = $dayMap[$taskCreationDate->dayOfWeek];
                $taskFinishDay = $dayMap[$taskUpdateDate->dayOfWeek];

                    if($userTask['progress_percentage'] == 100)
                        $statisticsData['finishedTasks'][$taskFinishDay] += 1;
                        
                $statisticsData['createdTasks'][$taskCreationDate] += 1;
            }
        }

        return $statisticsData;
    }

    public function notifications() {

        $todayTasks = Task::todayDeadlineTasks()->get()->toArray();
        $todayExams = Exam::todayExams()->get()->toArray();

        $todayEvents = Helpers::processEvents($todayExams, $todayTasks);

        $scheduleItems = $this->calendarData()['currentDayScheduleItems'];
        $userTime = Carbon::now()->tz($this->clientTimeZone);
        $userTime = Carbon::now()->tz($this->clientTimeZone);

        $notifications = [
        ];  

        foreach($todayEvents as $event) {
            if(($event['subtitle'] == 'Exam')) {
                if($event['alert_ack'])
                    continue;

                $splittedPeriod = explode(' ', $event['period']);
                $examStartTime = $splittedPeriod[1];

                $userTime->setTimeFromTimeString($examStartTime);

                array_push($notifications, [
                    "id" => $event['id'],
                    "remainedAt" => $userTime->timestamp,
                    "eventType" => 'Exam',
                    "dueDate" => $examStartTime,
                    "title" => $event['title']
                ]);
            }
            else {
                if($event['alert_ack']) 
                    continue;
                
                $splittedPeriod = explode(' ', $event['period']);
                $userTime->setTimeFromTimeString($splittedPeriod[1]);

                array_push($notifications, [
                    'id' => $event['id'],
                    "remainedAt" => $userTime->timestamp,
                    "eventType" => 'Task',
                    "dueDate" => $splittedPeriod[1],
                    "title" => $event['title']
                ]);
            }
        }

        foreach($scheduleItems as $scheduleItem) {
            if($scheduleItem['alert_ack'])
                continue;

            $splittedPeriod = explode(';', $scheduleItem['start_hour']);
            $courseStartHour = $splittedPeriod[0];

            $userTime->setTimeFromTimeString($courseStartHour);
            if(Carbon::now()->tz($this->clientTimeZone)->lt($userTime)) {

                array_push($notifications, [
                    "id" => $scheduleItem['id'],
                    "remainedAt" => $userTime->timestamp,
                    "eventType" => 'Course',
                    "dueDate" => $courseStartHour,
                    "title" => $scheduleItem['title']
                ]);
            }
        }

        return $notifications;
    }

    public function calendarData() {
        
        $bundledData = [
            "currentDayScheduleItems" => [],
            "scheduleItems" => [],
            "tasks" => [],
            "exams" => []
        ];

        try {
            $scheduleItems = $this->schedule->items->toArray();
            $currentDayTime = null;
         
            if($this->clientTimeZone) {
                $currentDayTime = Carbon::now()->tz($this->clientTimeZone)->format('Y-m-d H:i:s');
            }
            else{
                $currentDayTime = Carbon::now()->format('Y-m-d H:i:s');
            }
            
            $bundledData['tasks'] = $this->latestTasks($currentDayTime);
            $bundledData['exams'] = $this->latestExams($currentDayTime);
    
            if(count($scheduleItems)) {
                $dayMap = [
                    0 => 6,
                    1 => 0,
                    2 => 1,
                    3 => 2,
                    4 => 3,
                    5 => 4,
                    6 => 5
                ];
    
                $currentDay = $dayMap[Carbon::parse($currentDayTime)->dayOfWeek];
                $currentHour = Carbon::now()->tz($this->clientTimeZone);
    
                for($i = 0; $i < count($scheduleItems); ++$i) {
                    for($j = 0 + $i; $j < count($scheduleItems); ++$j) {
                        $iStartHour = Carbon::parse($scheduleItems[$i]['start_hour']);
                        $jStartHour = Carbon::parse($scheduleItems[$j]['start_hour']);
    
                        if($jStartHour->lt($iStartHour)) {
                            $temp = $scheduleItems[$i];
                            $scheduleItems[$i] = $scheduleItems[$j];
                            $scheduleItems[$j] = $temp;
                        }
                    };
                };
    
                for($i = 0; $i < count($scheduleItems) && count($bundledData['currentDayScheduleItems']) < 4; ++$i){
                    if(($scheduleItems[$i]['day'] == $currentDay))
                    {
                        $splittedScheduleItemEndHour = explode(':', $scheduleItems[$i]['end_hour']);
                        $hourOfScheduleItemEndHour = (int) $splittedScheduleItemEndHour[0];
                        $minutesOfScheduleItemEndHour = (int) $splittedScheduleItemEndHour[1];
    
                        $scheduleItem = Carbon::now()->tz($this->clientTimeZone);
    
                        $scheduleItem->setTime($hourOfScheduleItemEndHour, $minutesOfScheduleItemEndHour);
                        if($scheduleItem->gt($currentHour)) {
                            $scheduleItems[$i]['start_hour'] = Carbon::parse($scheduleItems[$i]['start_hour'])->format('H:i');
                            $scheduleItems[$i]['end_hour'] = Carbon::parse($scheduleItems[$i]['end_hour'])->format('H:i');
                            array_push($bundledData['currentDayScheduleItems'], $scheduleItems[$i]);
                        }
                    }
                }
                
                for($i = 0; $i < count($bundledData['currentDayScheduleItems']); ++$i) {
                    for($j = 0 + $i; $j < count($bundledData['currentDayScheduleItems']); ++$j) {
                        $iScheduleItem = $bundledData['currentDayScheduleItems'][$i];
                        $jScheduleItem = $bundledData['currentDayScheduleItems'][$j];
                        
                        $iSplittedScheduleItemEndHour = explode(':', $iScheduleItem['end_hour']);
                        $jSplittedScheduleItemEndHour = explode(':', $jScheduleItem['end_hour']);
    
                        $iHourOfScheduleItemEndHour = (int) $iSplittedScheduleItemEndHour[0];
                        $iMinutesOfScheduleItemEndHour = (int) $iSplittedScheduleItemEndHour[1];
    
                        $iScheduleItemDate = Carbon::now()->tz($this->clientTimeZone);
                        $iScheduleItemDate->setTime($iHourOfScheduleItemEndHour, $iMinutesOfScheduleItemEndHour);
    
                        $jHourOfScheduleItemEndHour = (int) $jSplittedScheduleItemEndHour[0];
                        $jMinutesOfScheduleItemEndHour = (int) $jSplittedScheduleItemEndHour[1];
    
                        $jScheduleItemDate = Carbon::now()->tz($this->clientTimeZone);
                        $jScheduleItemDate->setTime($jHourOfScheduleItemEndHour, $jMinutesOfScheduleItemEndHour);
    
                        if($iScheduleItemDate->gt($jScheduleItemDate)) {
                            $temp = $bundledData['currentDayScheduleItems'][$j];
                            $bundledData['currentDayScheduleItems'][$j] = $bundledData['currentDayScheduleItems'][$i];
                            $bundledData['currentDayScheduleItems'][$i] = $temp;
                        }
                    }                   
                }
            }
        }catch(Throwable $error) {
            // ignore
        }

        return $bundledData;
    }

    public function latestExams($date, $intervalDate = null) {
        if(!isset($intervalDate))
            return Exam::where('user_id', $this->id)
                ->where('date', '>', $date)
                ->get()
                ->toArray();

        return Exam::where('user_id', $this->id)
        ->whereBetween('date', [$date, $intervalDate])
        ->get()
        ->toArray();
    }

    public function latestTasks($date, $intervalDate = null) {
        if(!isset($intervalDate))
            return Task::where('user_id', $this->id)
            ->where('deadline', '>', $date)
            ->get()
            ->toArray();

        return Task::where('user_id', $this->id)
            ->whereBetween('deadline', [$date, $intervalDate])
            ->get()
            ->toArray();
    }
}
