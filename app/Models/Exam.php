<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Exam extends Model
{
    use HasFactory;

    protected $table = 'exams';

    protected $fillable = [
        'user_id',
        'course_name',
        'class_name',
        'teacher_name',
        'date',
        'duration',
        'calendar_color',
        'box_color',
        'passed',
        'alert_ack'
    ];

    public static function getByInterval($start_date, $end_date) {
        return Exam::where('user_id', auth()->id())
                ->whereBetween('date', [ $start_date, $end_date ]);
    }

    public static function todayExams() {
        $clientTimeZone = auth()->user()->client_time_zone;

        $currentDayTime = Carbon::now()->tz($clientTimeZone)->format('Y-m-d H:i:s');
        $todayEnd = Carbon::parse($currentDayTime)->endOfDay()->format('Y-m-d H:i:s');
        
        return Exam::where('user_id', auth()->id())
                    ->whereBetween('date', [$currentDayTime, $todayEnd]);
    }
    
}
