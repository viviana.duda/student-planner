<?php

namespace App\Models;

use App\Models\TaskFile;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Task extends Model
{
    use HasFactory;
    protected $table = 'tasks';

    protected $fillable = [
        'user_id',
        'title',
        'course',
        'deadline',
        'description',
        'progress_percentage',
        'alert_ack'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function files() {
        return $this->hasMany(TaskFile::class, 'task_id');
    }

    public static function todayDeadlineTasks() {
        $clientTimeZone = auth()->user()->client_time_zone;

        $currentDayTime = Carbon::now()->tz($clientTimeZone)->format('Y-m-d H:i:s');
        $todayEnd = Carbon::parse($currentDayTime)->endOfDay()->format('Y-m-d H:i:s');
        
        return Task::where('user_id', auth()->id())
                    ->whereBetween('deadline', [$currentDayTime, $todayEnd]);
    }
    
    public static function getByInterval($start_date, $end_date) {
        return Task::where('user_id', auth()->id())
                    ->whereBetween('deadline', [$start_date, $end_date]);
    }
}
