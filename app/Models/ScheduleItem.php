<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleItem extends Model
{
    use HasFactory;
    protected $table = 'schedule_items';

    protected $fillable = [
        'schedule_id',
        'day',
        'start_hour',
        'end_hour',
        'title'
    ];
}
